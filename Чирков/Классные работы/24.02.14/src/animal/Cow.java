package animal;

import food.Food;

/**
 * Created by ilya on 24.02.15.
 */
public class Cow extends Animal {

    @Override
    public void eat(Food f) {
        if (f == Food.MEAT) throw new IllegalArgumentException("I can't eat meat!");
        else if (f == Food.GRASS) System.out.println("Nice grass!");
    }
}
