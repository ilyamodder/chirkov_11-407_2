package animal;

import food.Food;

import java.util.Random;

/**
 * Created by ilya on 24.02.15.
 */
public abstract class Animal {
    public void eat() {}
    public abstract void eat(Food f);

    public static Animal getRandomAnimal() {
        Random random = new Random();
        switch (random.nextInt(3)) {
            case 0:
                return new Cow();
            case 1:
                return new Tiger();
            default:
                return new Bear();
        }
    }
}
