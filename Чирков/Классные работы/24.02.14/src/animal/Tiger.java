package animal;

import food.Food;

/**
 * Created by ilya on 24.02.15.
 */
public class Tiger extends Animal {
    @Override
    public void eat(Food f) {
        if (f == Food.GRASS) throw new IllegalArgumentException("I can't eat grass!");
        else if (f == Food.MEAT) System.out.println("Nice meat, thanks!");
    }
}
