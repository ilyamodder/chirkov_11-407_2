/**
 * Created by ilya on 24.02.15.
 */
public class CustomException {
    public static void main(String[] args) {
        try {
            System.out.println(plus(3,3));
        } catch (ThreeArgumentException e) {
            System.out.println("Аргумент равен 3!");
        }
    }

    public static int plus (int a, int b) throws ThreeArgumentException {
        if ((a == 3) || (b == 3)) throw new ThreeArgumentException();
        else return a + b;
    }
}
