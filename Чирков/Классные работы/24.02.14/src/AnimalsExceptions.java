import animal.Animal;
import food.Food;

/**
 * Created by ilya on 24.02.15.
 */
public class AnimalsExceptions {
    public static void main(String[] args) {
        Animal[] arr = new Animal[5];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = Animal.getRandomAnimal();
            try {
                arr[i].eat(Food.MEAT);
            } catch (IllegalArgumentException e) {
                //System.err.println("Ошибка: " + e.getLocalizedMessage());
                e.printStackTrace();
            }
        }


    }
}
