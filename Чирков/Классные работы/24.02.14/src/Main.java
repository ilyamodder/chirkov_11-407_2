import animal.Animal;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by ilya on 24.02.15.
 */
public class Main {
    public static void main(String[] args) {
        try {
            Animal x = null;
            x.eat();
        } catch (Exception e) {
            System.out.println(e.toString());
        }

        test1();

        int a = 5/((int)((Integer)null));
    }

    public static void test1() {
        FileInputStream stream = null;
        try {
            stream = new FileInputStream("nothing");
            stream.read();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void test2() throws IOException {
        FileInputStream stream = new FileInputStream("nothing");
        stream.read();
    }

}
