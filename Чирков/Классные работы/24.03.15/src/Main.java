import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

/**
 * Created by ilya on 24.03.15.
 */
public class Main {
    private static final int PORT = 15097;
    private static final String IP = "10.17.12.235";
    public static void main(String[] args) {
        try (DatagramSocket receiveSocket = new DatagramSocket(15097)) {
            DatagramPacket packet = new DatagramPacket(new byte[1024], 1024);
            for (int i = 0; i < 1000; i++) {
                byte[] arr = (i + ".txt").getBytes();
                DatagramPacket sendPacket = new DatagramPacket(arr, arr.length);
                DatagramSocket sendSocket = new DatagramSocket();
                sendSocket.connect(new InetSocketAddress(IP, 15097));
                sendSocket.send(sendPacket);
                receiveSocket.receive(packet);
                System.out.println(i + " " + new String(packet.getData()).trim());
                
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
