package sync;

/**
 * Created by ilya on 24.03.15.
 */
public class UsainBolt extends Thread implements Sportsman {

    @Override
    public void doSprint() {
        start();
    }

    @Override
    public void run() {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
