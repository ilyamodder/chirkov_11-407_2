import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ilya on 14.03.15.
 */
public class HashSetTest {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        for (int i = 0; i < 10; i++) {
            set.add("Элемент " + i);
        }
        System.out.println(set.add("Элемент 1"));
        System.out.println(Arrays.toString(set.toArray()));
    }
}
