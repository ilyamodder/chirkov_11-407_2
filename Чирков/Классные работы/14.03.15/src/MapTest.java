import animal.Animal;
import animal.Cat;
import animal.Dog;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ilya on 14.03.15.
 */
public class MapTest {
    public static void main(String[] args) {
        Map<String, Animal> map = new HashMap<>();
        map.put("Bobik", new Dog());
        map.put("Murka", new Cat());
        Animal animal = map.get("Bobik");
    }
}
