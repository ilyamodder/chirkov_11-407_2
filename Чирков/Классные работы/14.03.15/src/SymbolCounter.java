import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Created by ilya on 14.03.15.
 */
public class SymbolCounter {
    public static void main(String[] args) {
        File file = new File("file.txt");
        Set<Character> ignored = new HashSet<>();
        ignored.add(',');
        ignored.add('.');
        ignored.add('-');
        ignored.add('\n');
        ignored.add(' ');
        ignored.add(')');
        ignored.add('(');
        try {
            List<String> lines = Files.readAllLines(file.toPath());
            Map<Character, Integer> counts = new TreeMap<>();
            lines.forEach(line -> {
                for (int i = 0; i < line.length(); i++) {
                    Character c = line.charAt(i);
                    if (!ignored.contains(c)) {
                        Integer count = counts.get(c);
                        count = count==null ? 1 : count+1;
                        counts.put(c, count);
                    }
                }
            });

            System.out.println(counts.toString());
        } catch (IOException e) {
            System.out.println("Ошибка чтения из файла " + file.getAbsolutePath());
        }
    }
}
