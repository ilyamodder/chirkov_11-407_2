package ru.kpfu.chirkov.view;

import ru.kpfu.chirkov.model.Disco;
import ru.kpfu.chirkov.model.Music;
import ru.kpfu.chirkov.model.Person;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by ilya on 21.04.15.
 */
public class View {
    Disco disco;
    Scanner input;

    public View(Disco disco) {
        this.disco = disco;
        input = new Scanner(System.in);
        Set<Music> musicSet = new HashSet<>();
        musicSet.add(Music.POP);
        musicSet.add(Music.RAP);
        Person person = new Person(musicSet, "Валерий");
        person.setStateChangedConsumer(this::onPersonStateChanged);
        disco.addPerson(person);
    }

    public void processCommands() {
        while (true) {
            System.out.println("Введите жанр музыки");
            Music music = Music.valueOf(input.nextLine().toUpperCase());
            disco.changeMusic(music);
        }
    }

    public void onPersonStateChanged(Person person) {
        System.out.println(person.getName() + " is " + person.getState().name().toLowerCase() + " with " + person.getDancingPart().name().toLowerCase());
    }
}
