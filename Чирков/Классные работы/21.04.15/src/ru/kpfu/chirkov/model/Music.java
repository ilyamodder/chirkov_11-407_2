package ru.kpfu.chirkov.model;

/**
 * Created by ilya on 21.04.15.
 */
public enum Music {
    POP, ROCK, RAP, HIP_HOP
}
