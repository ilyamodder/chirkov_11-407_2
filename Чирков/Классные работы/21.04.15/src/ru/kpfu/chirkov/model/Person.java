package ru.kpfu.chirkov.model;

import com.sun.xml.internal.ws.client.sei.ResponseBuilder;

import java.util.Set;
import java.util.function.Consumer;

/**
 * Created by ilya on 21.04.15.
 */
public class Person {
    private Set<Music> favouriteMusic;
    private State state;
    private Consumer<Person> stateChangedConsumer;
    private String name;
    private BodyPart dancingPart;

    public Person(Set<Music> favouriteMusic, String name) {
        this.favouriteMusic = favouriteMusic;
        this.name = name;
    }

    public void onMusicChanged(Music currentMusic) {
        State newState = favouriteMusic.contains(currentMusic) ? State.DANCING : State.DRINKING;
        if (newState.equals(State.DANCING)) {
            dancingPart = BodyPart.getByMusic(currentMusic);
        }
        setState(newState);
    }

    public State getState() {
        return state;
    }

    private void setState(State state) {
        if (!state.equals(this.state)) {
            this.state = state;
            stateChangedConsumer.accept(this);
        } else {
            this.state = state;
        }

    }

    public void setStateChangedConsumer(Consumer<Person> stateChangedConsumer) {
        this.stateChangedConsumer = stateChangedConsumer;
    }

    public String getName() {
        return name;
    }

    public BodyPart getDancingPart() {
        return dancingPart;
    }
}
