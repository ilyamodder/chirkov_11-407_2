package ru.kpfu.chirkov.model;

/**
 * Created by ilya on 21.04.15.
 */
public enum BodyPart {
    HEAD, ARMS, FEET, STOMACH;
    public static BodyPart getByMusic(Music music) {
        switch (music) {
            case ROCK: return HEAD;
            case RAP: return FEET;
            case HIP_HOP: return ARMS;
            case POP: return STOMACH;
        }
        return null;
    }
}
