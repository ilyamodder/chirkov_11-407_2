package ru.kpfu.chirkov;

import ru.kpfu.chirkov.model.Disco;
import ru.kpfu.chirkov.view.View;

/**
 * Created by ilya on 21.04.15.
 */
public class Main {
    public static void main(String[] args) {
        View v = new View(new Disco());
        v.processCommands();
    }
}
