import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by ilya on 17.03.15.
 */
public class MyDictionary implements PBook {
    public static void main(String[] args) {
        MyDictionary dictionary = new MyDictionary();
        Scanner scanner = new Scanner(System.in);
        try {
            dictionary.readFromFile(new File("data.txt"));
            while (true) {
                System.out.println("Введите имя или /q для выхода:");
                String input = scanner.nextLine();
                if (input.equals("/q")) break;

                String number = dictionary.getNumber(scanner.nextLine());
                System.out.println(number != null ? "Телефон: " + number : "Не найдено");
            }
        } catch (IOException e) {
            System.out.println("Ошибка ввода-вывода");
        }
    }

    private Map<String, String> dict;
    File file;

    public MyDictionary() {
        dict = new HashMap<>();
    }

    public void readFromFile(File file) throws IOException {
        this.file = file;
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;

            while ((line = reader.readLine()) != null) {
                String[] data = line.split(",");
                dict.put(data[0], data[1]);
            }
        }
    }

    public String getNumber(String name) {
        return dict.get(name);
    }

    @Override
    public String getNumber2(String name){
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;

            while ((line = reader.readLine()) != null) {
                String[] data = line.split(",");
                if (data[0].equals(name)) return data[1];
            }
            return null;
        } catch (IOException e) {
            return null;
        }
    }
}
