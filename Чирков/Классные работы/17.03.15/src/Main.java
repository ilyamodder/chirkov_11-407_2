import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by ilya on 17.03.15.
 */
public class Main {
    public static void main(String[] args) {
        try (FileDictionary dictionary = new FileDictionary(new File("data.txt"))) {
            System.out.println(dictionary.getNumber("Илья"));
        } catch (IOException e) {
            System.out.println("Ошибка ввода-вывода");
        }
    }
}
