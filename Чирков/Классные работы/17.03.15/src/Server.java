import javax.xml.crypto.Data;
import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;


/**
 * Created by ilya on 17.03.15.
 */
public class Server {
    public static void main(String[] args) {
        MyDictionary dictionary = new MyDictionary();
        try (DatagramSocket socket = new DatagramSocket(5050)) {
            dictionary.readFromFile(new File("data.txt"));
            DatagramPacket sizePacket;
            DatagramPacket dataPacket;
            while (true) {
                sizePacket = new DatagramPacket(new byte[1], 1);
                socket.receive(sizePacket);
                byte[] length = sizePacket.getData();
                byte[] data = new byte[length[0]];
                dataPacket = new DatagramPacket(data, data.length);
                socket.receive(dataPacket);

                socket.connect(new InetSocketAddress(sizePacket.getAddress(), 5050));

                String received = new String(dataPacket.getData());
                System.out.println("Пришло: " + received);
                System.out.println(dataPacket.getAddress().getHostName() + ":" + 5050);
                String number = dictionary.getNumber(received);
                System.out.println("Отправляем: " + number);

                length[0] = number != null ? (byte) number.length() : 0;
                sizePacket = new DatagramPacket(length, length.length);
                socket.send(sizePacket);
                dataPacket = new DatagramPacket(number.getBytes(), number.getBytes().length);
                socket.send(dataPacket);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
