import java.io.*;

/**
 * Created by ilya on 17.03.15.
 */
public class FileDictionary implements Closeable {
    private BufferedReader reader;
    public FileDictionary(File file) throws FileNotFoundException {
        reader = new BufferedReader(new FileReader(file));
    }

    public String getNumber(String number) throws IOException {
        String line;
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            if (data[0].equals(number)) return data[1];
        }
        return null;
    }

    @Override
    public void close() throws IOException {
        if (reader != null) reader.close();
    }
}
