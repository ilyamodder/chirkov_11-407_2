import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

/**
 * Created by ilya on 17.03.15.
 */
public class Client {
    public static void main(String[] args) {
        MyDictionary dictionary = new MyDictionary();
        try (DatagramSocket socket = new DatagramSocket(5050)) {
            String name = "Илья";
            byte[] length = {(byte) name.length()};
            DatagramPacket sizePacket = new DatagramPacket(length, 1);
            DatagramPacket dataPacket = new DatagramPacket(name.getBytes(), name.length());
            socket.connect(new InetSocketAddress("localhost", 5051));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
