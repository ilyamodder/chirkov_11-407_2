package ru.kpfu.chirkov.polynom;

/**
 * Created by ilya on 31.03.15.
 */
public class Polynom {
    double a;
    double b;
    double c;

    public Polynom(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    public double[] findResults() {
        double d = b*b - 4*a*c;
        if (d<0) return new double[0];
        else if (d==0) {
            double[] res = new double[1];
            res[0] = (-b)/(2*a);
            return res;
        } else {
            double[] res = new double[2];
            res[0] = (-b + Math.sqrt(d))/(2*a);
            res[1] = (-b - Math.sqrt(d))/(2*a);
            return res;
        }
    }
}
