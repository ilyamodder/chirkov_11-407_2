package ru.kpfu.chirkov.polynom;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ilya on 31.03.15.
 */
public class PolynomCalculator {
    Thread[] threads;
    List<Polynom> polynoms;

    public PolynomCalculator(Polynom... polynoms) {
        this.polynoms = Arrays.asList(polynoms);
        createThreads();
    }

    public PolynomCalculator(List<Polynom> list) {
        polynoms = list;
        createThreads();
    }

    private void createThreads() {
        int threadsCount = (int) Math.ceil(((double) polynoms.size()) / 10);
        threads = new Thread[threadsCount];
        for (int i = 0; i < threadsCount; i++) {
            threads[i] = new CalculationThread(polynoms.subList(10*i, Math.min(10*(i+1)-1, polynoms.size())));
        }
    }

    public void calculate() {
        for (Thread thread : threads) {
            thread.start();
        }
    }

    private class CalculationThread extends Thread {
        private List<Polynom> polynoms;

        public CalculationThread(List<Polynom> polynoms) {
            this.polynoms = polynoms;
        }

        @Override
        public void run() {
            polynoms.forEach(polynom -> System.out.println(Arrays.toString(polynom.findResults())));
        }
    }
}
