package ru.kpfu.chirkov.polynom;

import java.util.*;

/**
 * Created by ilya on 31.03.15.
 */
public class PolynomsMain {
    public static void main(String[] args) {
        List<Polynom> polynoms = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 1000; i++) {
            polynoms.add(new Polynom(random.nextInt(100), random.nextInt(100), random.nextInt(100)));
        }
        PolynomCalculator calculator = new PolynomCalculator(polynoms);
        calculator.calculate();
    }
}
