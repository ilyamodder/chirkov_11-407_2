package ru.kpfu.chirkov;

/**
 * Created by ilya on 31.03.15.
 */
public class ThreadMain {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            MyRunnable2 myRunnable = new MyRunnable2(i);
            myRunnable.start();
        }
    }

    public static void work(int i) {
        try {
            Thread.sleep(1000);
            System.out.println("Work " + i + " finished");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
