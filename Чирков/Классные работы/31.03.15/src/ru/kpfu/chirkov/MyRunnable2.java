package ru.kpfu.chirkov;

/**
 * Created by ilya on 31.03.15.
 */
public class MyRunnable2 extends Thread {
    int i;

    public MyRunnable2(int i) {
        this.i = i;
    }

    @Override
    public void run() {
        ThreadMain.work(i);
    }
}
