package ru.kpfu.chirkov.runners;

/**
 * Created by ilya on 31.03.15.
 */
public class UsainBolt extends Thread implements Sprinter {
    @Override
    public void run() {
        try {
            Thread.sleep((long) (9580 + Math.random() * 1000));
            System.out.println("Болт прибежал");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doSprint() {
        start();
    }
}
