package ru.kpfu.chirkov.runners;

/**
 * Created by ilya on 31.03.15.
 */
public interface Sprinter {
    void doSprint();
}
