package ru.kpfu.chirkov.runners;

/**
 * Created by ilya on 31.03.15.
 */
public class MichaelJohnson extends Thread implements Sprinter {
    @Override
    public void run() {
        try {
            Thread.sleep((long) (10090 + 1000 * Math.random()));
            System.out.println("Джонсон прибежал");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doSprint() {
        start();
    }
}
