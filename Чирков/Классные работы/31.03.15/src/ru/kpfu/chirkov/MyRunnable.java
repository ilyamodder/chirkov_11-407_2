package ru.kpfu.chirkov;

/**
 * Created by ilya on 31.03.15.
 */
public class MyRunnable implements Runnable {
    int i;

    public MyRunnable(int i) {
        this.i = i;
    }

    @Override
    public void run() {
        ThreadMain.work(i);
    }
}
