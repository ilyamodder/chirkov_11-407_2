package ru.kpfu.chirkov;

import ru.kpfu.chirkov.runners.AsafaPowell;
import ru.kpfu.chirkov.runners.MichaelJohnson;
import ru.kpfu.chirkov.runners.Sprinter;
import ru.kpfu.chirkov.runners.UsainBolt;

/**
 * Created by ilya on 31.03.15.
 */
public class RunnersTest {


    public static void main(String[] args) {
        Sprinter[] sprinters = new Sprinter[3];
        sprinters[0] = new UsainBolt();
        sprinters[1] = new AsafaPowell();
        sprinters[2] = new MichaelJohnson();
        System.out.println("Начинаем забег на 100 метров");
        for (Sprinter sprinter : sprinters) {
            sprinter.doSprint();
        }
    }
}
