/**
 * Created by ilya on 28.02.15.
 */
public class MyArrayList<E> implements MyList<E> {
    private int size;
    private Object[] data;

    public MyArrayList() {
        size = 0;
        data = new Object[10];
    }

    public MyArrayList(int size) {
        this.size = 0;
        data = new Object[size];
    }

    @Override
    public void add(int index, E value) {
        if (index == size()) add(value);
        else if (index < size()) {
            add(null);
            System.arraycopy(data, index, data, index + 1, size - 1 - index);
            data[index] = value;
        }
    }

    @Override
    public void add(E value) {
        if (data.length <= size()) {
            Object[] newData = new Object[data.length * 2];
            System.arraycopy(data, 0, newData, 0, data.length);
            data = newData;
        }
        data[size()] = value;
        size++;
    }

    @Override
    public void remove(int index) {
        if (index < size()) {
            System.arraycopy(data, index+1, data, index, size - index - 1);
            data[size()-1] = null;
            size--;
        } else throw new IndexOutOfBoundsException("size is " + size() + ", index is " + index);

    }

    @Override
    @SuppressWarnings("unchecked")
    public E get(int index) {
        return (E) data[index];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void addRange(MyList<E> list) {
        for (int i=0; i<list.size(); i++) {
            add(list.get(i));
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (int i = 0; i < size-1; i++) {
            builder.append(get(i).toString());
            builder.append(", ");
        }
        if (size != 0) builder.append(get(size() - 1));
        builder.append("]");
        return builder.toString();
    }
}
