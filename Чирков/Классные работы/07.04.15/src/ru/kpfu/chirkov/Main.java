package ru.kpfu.chirkov;

import ru.kpfu.chirkov.algorithms.BubbleSort;
import ru.kpfu.chirkov.algorithms.InsertSort;
import ru.kpfu.chirkov.algorithms.JavaSort;
import ru.kpfu.chirkov.algorithms.SimpleSelectSort;

import java.util.Random;

/**
 * Created by ilya on 07.04.15.
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        Sortable[] sortables = new Sortable[]{new BubbleSort(), new InsertSort(), new JavaSort(), new SimpleSelectSort()};
        SortingThread[] threads = new SortingThread[sortables.length];
        int[] arr = new int[100000];
        Random random = new Random();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = random.nextInt(500);
        }
        for (int i = 0; i < threads.length; i++) {
            int[] newArr = new int[arr.length];
            System.arraycopy(arr, 0, newArr, 0, arr.length);
            threads[i] = new SortingThread(sortables[i], newArr);
            threads[i].start();
        }
        for (SortingThread thread : threads) {
            thread.join();
        }

        for (SortingThread thread : threads) {
            System.out.println(thread.getSortName() + "\t" + thread.getTime());
        }
    }
}
