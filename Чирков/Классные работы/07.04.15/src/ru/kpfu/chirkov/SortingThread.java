package ru.kpfu.chirkov;

import java.util.Random;

/**
 * Created by ilya on 07.04.15.
 */
public class SortingThread extends Thread {
    Sortable sortable;
    long time;
    int[] arr;

    public SortingThread(Sortable sortable, int[] arr) {
        this.sortable = sortable;
        this.arr = arr;
    }

    @Override
    public void run() {
        long start = System.currentTimeMillis();
        sortable.sort(arr);
        time = System.currentTimeMillis() - start;
    }

    public long getTime() {
        return time;
    }

    public String getSortName() {
        return sortable == null ? "" : sortable.getClass().getSimpleName();
    }
}
