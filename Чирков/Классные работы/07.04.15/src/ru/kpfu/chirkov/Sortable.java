package ru.kpfu.chirkov;

/**
 * Created by ilya on 07.04.15.
 */
public interface Sortable {
    void sort(int[] arr);
}
