package ru.kpfu.chirkov.algorithms;

import ru.kpfu.chirkov.Sortable;

/**
 * Created by ilya on 07.04.15.
 */
public class InsertSort implements Sortable {
    @Override
    public void sort(int[] a) {
        for (int i = 0; i < a.length; i++) {
            int min = i;
            for (int j = i+1; j < a.length; j++) {
                if (a[j] < a[min]) min = j;
            }
            int tmp = a[min];
            for (int j = min; j>i; j--) {
                a[j] = a[j-1];
            }
            a[i] = tmp;
        }
    }
}
