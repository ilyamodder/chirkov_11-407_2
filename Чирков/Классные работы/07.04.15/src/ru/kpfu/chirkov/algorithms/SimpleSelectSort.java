package ru.kpfu.chirkov.algorithms;

import ru.kpfu.chirkov.Sortable;

/**
 * Created by ilya on 07.04.15.
 */
public class SimpleSelectSort implements Sortable {
    @Override
    public void sort(int[] a) {
        for (int i=0; i<a.length-1; i++) {
            int min = i;
            for (int j=i+1; j<a.length; j++) {
                if (a[j] < a[min]) min = j;
            }
            if (min!=i) {
                int c = a[min];
                a[min] = a[i];
                a[i] = c;
            }
        }
    }
}
