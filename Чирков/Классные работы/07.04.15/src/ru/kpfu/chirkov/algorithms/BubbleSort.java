package ru.kpfu.chirkov.algorithms;

import ru.kpfu.chirkov.Sortable;

/**
 * Created by ilya on 07.04.15.
 */
public class BubbleSort implements Sortable {
    @Override
    public void sort(int[] a) {
        for (int i = 0; i < a.length - 1; i++)
            //доходим только до n-i-1, так как
            //последующие элементы уже на своих местах
            for (int j = 0; j < a.length - 1 - i; j++)
                if (a[j] > a[j+1]) {
                    int c = a[j];
                    a[j] = a[j+1];
                    a[j+1] = c;
                }
    }
}
