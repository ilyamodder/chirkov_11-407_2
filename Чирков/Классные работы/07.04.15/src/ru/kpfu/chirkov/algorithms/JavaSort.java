package ru.kpfu.chirkov.algorithms;

import ru.kpfu.chirkov.Sortable;

import java.util.Arrays;

/**
 * Created by ilya on 07.04.15.
 */
public class JavaSort implements Sortable {
    @Override
    public void sort(int[] arr) {
        Arrays.sort(arr);
    }
}
