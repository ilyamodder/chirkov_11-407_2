package ru.kpfu.chirkov;

public interface MyList<E> {
    public void add(E e);
    public void add(int index, E e);
    public void remove(int index);
    public E get(int index);
    public int size();
}
