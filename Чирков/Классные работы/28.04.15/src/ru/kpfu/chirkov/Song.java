package ru.kpfu.chirkov;

import ru.kpfu.chirkov.model.Music;

/**
 * Created by ilya on 21.04.15.
 */
public class Song {
    Music music;

    public Song(Music music) {
        this.music = music;
    }

    public Music getMusic() {
        return music;
    }
}
