package ru.kpfu.chirkov.model.person;

import ru.kpfu.chirkov.model.BodyPart;
import ru.kpfu.chirkov.model.DancingSkill;
import ru.kpfu.chirkov.model.Music;
import ru.kpfu.chirkov.model.State;

import java.util.Set;
import java.util.function.Consumer;

/**
 * Created by ilya on 21.04.15.
 */
public abstract class Person {
    protected Set<Music> favouriteMusic;
    private State state;
    private Consumer<Person> stateChangedConsumer;
    private String name;
    private BodyPart dancingPart;
    private DancingSkill currentSkill;
    protected boolean drunk;

    public Person(Set<Music> favouriteMusic, String name) {
        this.favouriteMusic = favouriteMusic;
        this.name = name;
        drunk = false;
    }

    public void onMusicChanged(Music currentMusic) {
        if (favouriteMusic.contains(currentMusic)) {
            dance(currentMusic);
        } else {
            doSomethingElse();
        }
    }

    protected abstract void doSomethingElse();

    protected void dance(Music currentMusic) {
        setState(State.DANCING);
        dancingPart = getBodyPart(currentMusic);
        currentSkill = getDancingSkill(currentMusic);
    }

    public State getState() {
        return state;
    }

    protected void setState(State state) {
        if (!state.equals(this.state)) {
            this.state = state;
            stateChangedConsumer.accept(this);
        } else {
            this.state = state;
        }
    }

    public void setStateChangedConsumer(Consumer<Person> stateChangedConsumer) {
        this.stateChangedConsumer = stateChangedConsumer;
    }

    public String getName() {
        return name;
    }

    public BodyPart getDancingPart() {
        return dancingPart;
    }

    protected BodyPart getBodyPart(Music music) {
        switch (music) {
            case ROCK: return BodyPart.HEAD;
            case RAP: return BodyPart.FEET;
            case HIP_HOP: return BodyPart.ARMS;
            case POP: return BodyPart.STOMACH;
        }
        return null;
    }

    public DancingSkill getDancingSkill() {
        return currentSkill;
    }

    private DancingSkill getDancingSkill(Music music){
        if (drunk) return DancingSkill.GOOD;
        else return getDancingSkillByMusic(music);
    }

    protected abstract DancingSkill getDancingSkillByMusic(Music music);
    ;
}
