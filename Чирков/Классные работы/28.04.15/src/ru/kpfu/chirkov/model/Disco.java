package ru.kpfu.chirkov.model;

import ru.kpfu.chirkov.model.person.Person;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by ilya on 21.04.15.
 */
public class Disco {
    private Set<Person> persons;
    private Music currentMusic;

    public Disco() {
        persons = new HashSet<Person>();
    }

    public void addPerson(Person person) {
        persons.add(person);
    }

    public void removePerson(Person person) {
        persons.remove(person);
    }

    public void changeMusic(final Music newMusic) {
        currentMusic = newMusic;
        persons.forEach(person -> person.onMusicChanged(newMusic));
    }
}
