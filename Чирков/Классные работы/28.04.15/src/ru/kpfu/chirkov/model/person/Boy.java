package ru.kpfu.chirkov.model.person;

import ru.kpfu.chirkov.model.DancingSkill;
import ru.kpfu.chirkov.model.Music;
import ru.kpfu.chirkov.model.State;

import java.util.Set;

/**
 * Created by ilya on 28.04.15.
 */
public class Boy extends Person {
    public Boy(Set<Music> favouriteMusic, String name) {
        super(favouriteMusic, name);
    }

    @Override
    protected void doSomethingElse() {
        setState(State.DRINKING_VODKA);
        drunk = true;
    }

    @Override
    protected DancingSkill getDancingSkillByMusic(Music music) {
        switch (music) {
            case HIP_HOP:
            case POP:
                return DancingSkill.GOOD;
            default: return DancingSkill.BAD;
        }
    }
}
