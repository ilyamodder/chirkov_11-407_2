package ru.kpfu.chirkov.model.person;

import ru.kpfu.chirkov.model.BodyPart;
import ru.kpfu.chirkov.model.DancingSkill;
import ru.kpfu.chirkov.model.Music;
import ru.kpfu.chirkov.model.State;

import java.util.Set;

/**
 * Created by ilya on 28.04.15.
 */
public class Girl extends Person {
    public Girl(Set<Music> favouriteMusic, String name) {
        super(favouriteMusic, name);
    }

    @Override
    protected void doSomethingElse() {
        setState(State.DRINKING_MARTINI);
        drunk = true;
    }

    @Override
    protected BodyPart getBodyPart(Music music) {
        switch (music) {
            case ROCK: return BodyPart.FEET;
            case RAP: return BodyPart.HEAD;
            default: return super.getBodyPart(music);
        }
    }

    @Override
    protected DancingSkill getDancingSkillByMusic(Music music) {
        switch (music) {
            case POP: return DancingSkill.GOOD;
            default: return DancingSkill.BAD;
        }
    }
}
