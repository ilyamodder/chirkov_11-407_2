package ru.kpfu.chirkov.model.person;

import ru.kpfu.chirkov.model.Music;

import java.util.HashSet;

/**
 * Created by ilya on 28.04.15.
 */
public class BBoy extends Boy {
    public BBoy(String name) {
        super(new HashSet<Music>(){{
            add(Music.HIP_HOP);
        }}, name);
    }
}
