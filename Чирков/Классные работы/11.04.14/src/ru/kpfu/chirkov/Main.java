package ru.kpfu.chirkov;

/**
 * Created by ilya on 11.04.15.
 */
public class Main {
    public static void main(String[] args) {
        BinaryTree binaryTree = new BinaryTree();
        binaryTree.add(20, 20);
        binaryTree.add(100, 100);
        binaryTree.add(50, 50);
        binaryTree.add(0,0);
        binaryTree.add(500, 500);
        binaryTree.add(501, 501);
        binaryTree.add(-1, -1);
        binaryTree.walk();
        binaryTree.sout();
    }
}
