import java.util.EmptyStackException;

/**
 * Created by ilya on 21.02.15.
 */
public class StackImpl<E> implements Stack<E> {

    private class Element {
        private E element;
        private Element previous;

        public Element(E element, Element previous) {
            this.element = element;
            this.previous = previous;
        }

        public E getElement() {
            return element;
        }

        public Element getPrevious() {
            return previous;
        }
    }

    private int size;
    private Element last;


    public StackImpl() {
        size = 0;
        last = null;
    }

    @Override
    public void push(E value) {
        last = new Element(value, last);
        size++;
    }

    @Override
    public E pop() {
        if (size > 0) {
            E last = this.last.getElement();
            this.last = this.last.getPrevious();
            size--;
            return last;
        } else throw new EmptyStackException();
    }

    @Override
    public int size() {
        return size;
    }
}
