package ru.kpfu.chirkov;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by ilya on 18.04.15.
 */
public class Heap<E> implements PriorityQueue<E> {
    private ArrayList<Node> array;;

    private class Node {
        int priority;
        E e;

        public Node(E value, int priority) {
            this.e = value;
            this.priority = priority;
        }

        public int getPriority() {
            return priority;
        }

        public E getE() {
            return e;
        }
    }

    @SuppressWarnings("unchecked")
    public Heap() {
        array = new ArrayList<Node>();
    }

    @Override
    public E pop() {
        return null;
    }

    @Override
    public void push(E value, int priority) {
        Node node = new Node(value, priority);
        array.add(node);
        moveUp();
    }

    private void moveUp() {
        Node last = array.get(array.size() - 1);

    }

}
