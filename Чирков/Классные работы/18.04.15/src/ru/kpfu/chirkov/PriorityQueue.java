package ru.kpfu.chirkov;

/**
 * Created by ilya on 18.04.15.
 */
public interface PriorityQueue<E> {
    E pop();
    void push(E value, int priority);
}
