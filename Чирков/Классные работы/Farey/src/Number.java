/**
 * Created by ilya on 14.02.15.
 */
public class Number {

    private int pi;
    private int qi;

    public int getPi() {
        return pi;
    }

    public int getQi() {
        return qi;
    }

    public int getNext() {
        return next;
    }

    public Number(int pi, int qi) {
        this.pi = pi;
        this.qi = qi;

    }

    public void setNext(int next) {
        this.next = next;
    }

    private int next;

    public Number(int pi, int qi, int next) {
        this.pi = pi;
        this.qi = qi;
        this.next = next;
    }

    @Override
    public String toString() {
        return pi + "/" + qi;
    }
}
