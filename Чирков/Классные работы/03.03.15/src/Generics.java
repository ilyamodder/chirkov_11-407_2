import java.util.ArrayList;

/**
 * Created by ilya on 03.03.15.
 */
public class Generics {
    public static void main(String[] args) {
        ArrayList x = new ArrayList();
        x.add(2);
        x.add("2");
        x.forEach(System.out::println);

        ArrayList<String> list = new ArrayList<>();
        list.add("string1");
        list.add("123");
        list.forEach(System.out::println);
    }
}
