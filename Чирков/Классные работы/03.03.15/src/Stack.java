/**
 * Created by ilya on 21.02.15.
 */
public interface Stack<E> {
    public void push(E value);
    public E pop();
    public int size();
}
