/**
 * Created by ilya on 28.02.15.
 */
public interface MyList<E> {
    void add(int index, E value);
    void add(E value);
    void remove(int index);
    E get(int index);
    int size();
    void addRange(MyList<E> list);
}
