import java.math.BigInteger;

/**
 * Created by ilya on 03.03.15.
 */
public class BoxingUnboxing {
    public static void main(String[] args) {
        Integer x1 = 10;
        Integer x2 = 10;
        Integer x3 = 1000;
        Integer x4 = 1000;
        System.out.println(x1 == x2);
        System.out.println(x3 == x4);

        System.out.println(x1.equals(x2));
        System.out.println(x3.equals(x4));

        System.out.println(Integer.parseInt("-123"));

        Double d1 = 2.2;
        Double d2 = new Double(2.2);
        Double d3 = Double.parseDouble("2.2");

        System.out.println(d1);
        System.out.println(d2);
        System.out.println(d3);


    }
}
