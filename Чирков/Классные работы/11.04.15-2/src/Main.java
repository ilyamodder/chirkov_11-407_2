import ru.kpfu.chirkov.Cat;
import ru.kpfu.chirkov.Person;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ilya on 11.04.15.
 */
public class Main {
    public static void main(String[] args) {
        Map<Cat, Person> x = new HashMap<>();
        Cat cat1 = new Cat("Мася");
        Cat cat2 = new Cat("Мася");
        x.put(cat1, new Person("Еламан"));
        x.put(cat2, new Person("Илья"));
        System.out.println(x.size());
        System.out.println(x.get(cat1).getName());
        System.out.println(x.get(cat2).getName());

        //дз: больница - принять кровь и выдать деньги
        //если такой человек уже был, то не берем кровь, говорим, что нужно прийти через N дней или там еще когда-нибудь
    }
}
