package ru.kpfu.chirkov;

/**
 * Created by ilya on 11.04.15.
 */
public class Person {
    private String name;

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
