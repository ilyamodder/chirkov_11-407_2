package ru.kpfu.chirkov;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by ilya on 25.04.15.
 */
public class Translator implements ITranslator {
    private Map<String, Dictionary> dictionaries;

    private Translator() throws IOException {
        dictionaries = new TreeMap<>();
        switchOn("default");
    }

    @Override
    public String[] translate(String text) {
        List<String> words = new LinkedList<>();
        dictionaries.entrySet().forEach(entry -> {
            Dictionary dictionary = entry.getValue();
            if (dictionary.containsWord(text)) {
                String[] translations = dictionary.get(text);
                Collections.addAll(words, translations);
            }
        });
        return words.toArray(new String[words.size()]);
    }

    @Override
    public void switchOn(String name) throws IOException {
        if (!dictionaries.containsKey(name)) {
            List<String> lines = Files.readAllLines(Paths.get(name + ".txt"));
            final Dictionary dictionary = new Dictionary();
            lines.forEach(line -> {
                String[] parsed = line.split("\t");
                String[] translations = Arrays.copyOfRange(parsed, 1, parsed.length);
                dictionary.put(parsed[0], translations);
            });
            dictionaries.put(name, dictionary);
        } else {
            dictionaries.get(name).setEnabled(true);
        }
    }

    @Override
    public void switchOff(String name) {
        if (dictionaries.containsKey(name)) dictionaries.get(name).setEnabled(false);
    }

    @Override
    public void addEntry(String original, String translation) {
        Dictionary dictionary = dictionaries.get("default");
        dictionary.addWord(original, translation);
    }
}
