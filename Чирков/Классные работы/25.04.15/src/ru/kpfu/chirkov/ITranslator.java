package ru.kpfu.chirkov;

import java.io.IOException;

/**
 * Created by ilya on 25.04.15.
 */
public interface ITranslator {
    String[] translate(String text);
    void switchOn(String name) throws IOException;
    void switchOff(String name);
    void addEntry(String original, String translate);
}
