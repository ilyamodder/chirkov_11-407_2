package ru.kpfu.chirkov;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by ilya on 25.04.15.
 */
public class Dictionary {
    Map<String, String[]> map;
    boolean enabled;

    public Dictionary(Map<String, String[]> map, boolean enabled) {
        this.map = map;
        this.enabled = enabled;
    }

    public Dictionary() {
        map = new TreeMap<>();
        enabled = true;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public Map<String, String[]> getMap() {
        return map;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String[] get(String word) {
        return map.get(word);
    }

    public void put(String word, String[] translations) {
        map.put(word, translations);
    }

    public boolean containsWord(String word) {
        return map.containsKey(word);
    }

    public void addWord(String original, String translation) {
        if (containsWord(original)) {
            String[] translations = get(original);
            String[] newTranslations = new String[translations.length + 1];
            System.arraycopy(translations, 0, newTranslations, 0, translations.length);
            map.put(original, newTranslations);
        }
    }
}
