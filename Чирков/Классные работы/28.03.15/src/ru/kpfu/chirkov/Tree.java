package ru.kpfu.chirkov;

/**
 * Created by ilya on 28.03.15.
 */
public class Tree {

    private class Node {
        private Node left;
        private Node right;
        private double value;
        private int key;

        public Node(int key, double value) {
            this.value = value;
            this.key = key;
        }

        public Node getLeft() {
            return left;
        }

        public Node getRight() {
            return right;
        }

        public double getValue() {
            return value;
        }

        public int getKey() {
            return key;
        }

        public void setLeft(Node left) {
            this.left = left;
        }

        public void setRight(Node right) {
            this.right = right;
        }

        public void setValue(double value) {
            this.value = value;
        }
    }

    private Node root;

    public Tree() {}
    public void add(int key, double value) {
        Node newNode = new Node(key, value);
        if (root == null) {
            root = newNode;
        } else {
            add(root, newNode);
        }
    }

    private void add(Node root, Node node) {
        if (node.getKey() > root.getKey()) {
            if (root.getRight() == null) {
                root.setRight(node);
            } else {
                add(root.getRight(), node);
            }
        } else if (node.getKey() < root.getKey()) {
            if (root.getLeft() == null) {
                root.setLeft(node);
            } else {
                add(root.getLeft(), node);
            }
        } else {
            root.setValue(node.getValue());
        }
    }

    public void remove(int key) {
        Node node = get(root, key);
    }

    public double get(int key) {
        if (root == null) return Double.NaN;
        else {
            Node found = get(root, key);
            return found == null ? Double.NaN : found.getValue();
        }
    }

    private Node get(Node root, int key) {
        if ((root == null) || (root.getKey() == key)){
            return root;
        } else if (key > root.getKey()) {
            return get(root.getRight(), key);
        } else if (key < root.getKey()) {
            return get(root.getLeft(), key);
        }
    }
}
