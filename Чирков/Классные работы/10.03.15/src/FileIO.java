import java.io.*;
import java.util.Scanner;

/**
 * Created by ilya on 10.03.15.
 */
public class FileIO {
    public static void main(String[] args) {
        File file = new File("test.txt");

        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            outputStream.write("Тест записи в файл".getBytes());
        } catch (IOException e) {
            System.out.println("Ошибка записи");
        }

        try (Scanner scanner = new Scanner(new FileInputStream(file))) {
            while (scanner.hasNext()) {
                System.out.println(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            System.out.println("Ошибка чтения: файл не найден");
        }
    }
}
