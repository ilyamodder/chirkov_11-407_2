import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;

/**
 * Created by ilya on 10.03.15.
 */
public class Sender {
    public static void main(String[] args) {
        try (DatagramSocket socket = new DatagramSocket()) {
            socket.connect(new InetSocketAddress("localhost", 5051));
            socket.send(new DatagramPacket("Тест".getBytes(), 0, "Tecт".getBytes().length));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
