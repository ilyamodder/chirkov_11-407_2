import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Created by ilya on 10.03.15.
 */
public class RandomAccess {
    public static void main(String[] args) {
        File file = new File("test.txt");

        try (RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw")) {
            for (int i = 0; i < 10; i++) {
                randomAccessFile.writeUTF("Строка");
            }
            randomAccessFile.seek(0);
            for (int i = 0; i < 10; i++) {
                String line = randomAccessFile.readUTF();
                System.out.println(line);
            }
        } catch (IOException e) {
            System.out.println("Ошибка ввода-вывода");
        }
    }
}
