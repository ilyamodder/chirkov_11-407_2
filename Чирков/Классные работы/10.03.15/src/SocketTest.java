import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.*;

/**
 * Created by ilya on 10.03.15.
 */
public class SocketTest {
    public static void main(String[] args) {
        try {
            DatagramSocket socket = new DatagramSocket(5051);
            //socket.connect(new InetSocketAddress("ttt", 5051));
            DatagramPacket packet = new DatagramPacket(new byte[100], 100);
            while (true){
                DatagramPacket packet2 = new DatagramPacket(new byte[100],100);
                socket.receive(packet2);
                System.out.println(new String(packet2.getData()));
            }

        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
