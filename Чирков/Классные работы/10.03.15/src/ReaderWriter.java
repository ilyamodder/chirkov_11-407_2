import java.io.*;

/**
 * Created by ilya on 10.03.15.
 */
public class ReaderWriter {
    public static void main(String[] args) {
        File file = new File("test.txt");
        final String str = "Тест записи в файл";

        try (FileWriter writer = new FileWriter(file)) {
            writer.write(str);
        } catch (IOException e) {
            System.out.println("Ошибка записи");
        }

        try (FileReader reader = new FileReader(file)) {
            char[] buf = new char[str.length()];
            reader.read(buf);
            System.out.println(new String(buf));
        } catch (IOException e) {
            System.out.println("Ошибка чтения");
        }
    }
}
