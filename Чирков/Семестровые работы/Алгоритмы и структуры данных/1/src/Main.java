import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by ilya on 26.02.15.
 */
public class Main {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Необходимо задать имя файла как аргумент командной строки");
        } else {
            try {
                Scanner input = new Scanner(new File(args[0]));
                SortedLinkedList list = new SortedLinkedList();
                while (input.hasNext()) {
                    list.add(input.nextInt());
                }
                System.out.println(list);
                list.reverse();
                System.out.println(list);
            } catch (FileNotFoundException e) {
                System.out.printf("Файл%sне найден%n", args[0]);
            }
        }
    }
}
