/**
 * Created by ilya on 26.02.15.
 */
public class SortedLinkedList {

    private class Element {
        private int element;
        private Element next;

        public Element(int element) {
            this.element = element;
            this.next = null;
        }

        public Element(int element, Element next) {
            this.element = element;
            this.next = next;
        }

        public int getElement() {
            return element;
        }

        public void setElement(int element) {
            this.element = element;
        }

        public Element getNext() {
            return next;
        }

        public void setNext(Element next) {
            this.next = next;
        }
    }

    private Element first, last;
    private int size;

    public SortedLinkedList() {
        size = 0;
        first = last = null;
    }

    public void add(int e) {
        Element newElement = new Element(e);
        if (getSize() == 0) {
            first = last = newElement;
        } else {
            Element previous = null;
            Element current = first;
            do {
                if (current.getElement() > e) {
                    if (previous != null) previous.setNext(newElement);
                    else first = newElement;

                    newElement.setNext(current);
                    break;
                }

                if (current == last) {
                    current.setNext(newElement);
                    last = newElement;
                    break;
                }
                previous = current;
                current = previous.getNext();
            } while (current != null);
        }
        size++;

    }

    public int getSize() {
        return size;
    }

    public int get(int position) {
        if (position >= getSize()) throw new IndexOutOfBoundsException();
        if (position == getSize() - 1) return last.getElement();

        Element current = first;
        for (int i=0; i<position; i++) {
            current = current.next;
        }
        return current.getElement();
    }

    public void reverse() {
        Element previous = null;
        Element current = first;
        Element next = current.getNext();
        for (int i=0; i<getSize()-1; i++) {
            current.setNext(previous);
            previous = current;
            current = next;
            next = current.getNext();
        }
        current.setNext(previous);
        Element tmp = first;
        first = last;
        last = tmp;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        Element current = first;
        for (int i=0; i<getSize()-1; i++) {
            builder.append(current.getElement());
            builder.append(" ");
            current = current.next;
        }
        builder.append(last.getElement());
        return builder.toString();
    }
}
