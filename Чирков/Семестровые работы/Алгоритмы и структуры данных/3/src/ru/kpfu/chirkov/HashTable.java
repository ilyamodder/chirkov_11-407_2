package ru.kpfu.chirkov;

import java.lang.reflect.Array;
import java.util.HashSet;
import java.util.Set;

/**
 * Hash table class
 */
public class HashTable<K, V> implements MyMap<K, V> {
    private class Node {
        K key;
        V value;
        Node next;
        public Node(K key, V value) {
            this.key = key;
            this.value = value;
            next = null;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }

        public Node getNext() {
            return next;
        }

        public void setValue(V value) {
            this.value = value;
        }

        public void setNext(Node next) {
            this.next = next;
        }

    }

    Node[] array;

    Set<K> keySet;

    /**
     * Creates a hash table
     * @param size size of internal array
     */
    @SuppressWarnings("unchecked")
    public HashTable(int size) {
        if (size < 1) throw new IllegalArgumentException("size must be greater than 0");
        array = (Node[]) Array.newInstance(Node.class, size);
        keySet = new HashSet<>();
    }

    public HashTable(MyMap<? extends K, ? extends V> map, int size) {
        this(size);
        if (map == null) throw new NullPointerException("map cannot be null");
        putAll(map);
    }

    private <K1 extends K, V1 extends V> void putAll(MyMap<K1, V1> map) {
        for (K1 key : map.keySet()) {
            put(key, map.get(key));
        }
    }

    /**
     * Returns size of map
     * @return size
     */
    @Override
    public int size() {
        return keySet.size();
    }

    /**
     * Gets a value from hashtable
     * @param key key
     * @return value
     */
    @Override
    public V get(K key) {
        if (key == null) throw new NullPointerException("key cannot be null");
        if (keySet.contains(key)) {
            Node node = array[getHash(key)];
            if (node == null) return null;
            while (node != null) {
                if (node.getKey().equals(key)) return node.getValue();
                node = node.getNext();
            }
        }
        return null;
    }

    /**
     * Puts a value into the table
     * @param key key
     * @param value value
     */
    @Override
    public void put(K key, V value) {
        if (key == null) throw new NullPointerException("key cannot be null");
        int hash = getHash(key);
        Node node = findParent(key, hash);
        if (node == null) {
            array[hash] = new Node(key, value);
        }
        else if (node.getKey().equals(key)) {
            node.setValue(value);
        }
        else if (node.getNext() != null) {
            node.getNext().setValue(value);
        }
        else {
            node.setNext(new Node(key, value));
        }
        keySet.add(key);
    }

    /**
     * Removes a value
     * @param key key
     */
    @Override
    public void remove(K key) {
        if (key == null) throw new NullPointerException("key cannot be null");
        int hash = getHash(key);
        Node node = array[hash];
        if (node != null) {
            Node previous = null;
            while (node != null) {
                if (node.getKey().equals(key)) {
                    if (previous == null) array[hash] = node.getNext();
                    else previous.setNext(node.getNext());
                    break;
                }
                previous = node;
                node = node.getNext();
            }
        }
        keySet.remove(key);
    }

    /**
     * Clears the hashtable
     */
    public void clear() {
        keySet.forEach(this::remove);
    }

    public Set<K> keySet() {
        return keySet;
    }

    public boolean contains(K key) {
        return keySet.contains(key);
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Swaps the other map's and this map's entries
     * @param otherMap other map
     */
    public void swap(MyMap<K, V> otherMap) {
        if (otherMap == null) throw new NullPointerException("map cannot be null");
        Set<K> otherKeySet = otherMap.keySet();
        MyMap<K, V> tempMap = new HashTable<K, V>(otherKeySet.size());
        for (K key : otherKeySet) {
            tempMap.put(key, otherMap.get(key));
        }
        otherMap.clear();
        for (K key : keySet()) {
            otherMap.put(key, get(key));
        }
        clear();
        for (K key : tempMap.keySet()) {
            put(key, tempMap.get(key));
        }

    }

    /**
     * Finds a parent of node which contains the key
     * @param key key
     * @param hash hash of the key
     * @return parent of the node, null if the node is root
     */
    private Node findParent(K key, int hash) {
        Node node = array[hash];
        if (node == null) return null;
        Node previous = null;
        while (node != null) {
            if (node.getKey().equals(key)) return previous;
            previous = node;
            node = node.getNext();
        }
        return previous;
    }

    /**
     * Computes a hash of the key based on hashCode()
     * @param key key
     * @return hash [0..array.length]
     */
    private int getHash(K key) {
        return key.hashCode() % array.length;
    }
}
