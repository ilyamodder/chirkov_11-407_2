package ru.kpfu.chirkov;

import ru.kpfu.chirkov.HashTable;
import ru.kpfu.chirkov.MyMap;

/**
 * Created by ilya on 14.04.15.
 */
public class Main {
    public static void main(String[] args) {
        MyMap<String, String> map = new HashTable<>(1000);
        map.put("1", "!");
        map.put("2", "2");
        map.remove("1");
        System.out.println(map.get("2"));
    }
}
