package ru.kpfu.chirkov;

import java.util.Set;

/**
 * Created by ilya on 14.04.15.
 */
public interface MyMap<K, V> {
    int size();
    V get(K key);
    void put(K key, V value);
    void remove(K key);
    void clear();
    Set<K> keySet();
    boolean contains(K key);
    boolean isEmpty();
}
