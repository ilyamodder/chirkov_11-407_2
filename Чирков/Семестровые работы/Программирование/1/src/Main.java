import java.io.*;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Created by ilya on 19.03.15.
 */
public class Main {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Неверные аргументы командной строки. Должно быть задано только имя входного файла");
            return;
        }
        File file = new File(args[0]);
        if (!file.exists()) {
            System.out.printf("Файл %s не найден.", file.getPath());
            return;
        }
        List<String> lines = null;
        try {
            lines = Files.readAllLines(file.toPath());
            if (lines.size() == 0) {
                System.out.println("Файл пустой");
                return;
            }
        } catch (IOException e) {
            System.out.println("Ошибка ввода-вывода (возможно, файл был удален?)");
        }
        StringBuilder builder = new StringBuilder();
        for (String line : lines) {
            builder.append(line);
            builder.append("\n");
        }
        System.out.print("Построение суффиксного массива... ");
        SubstringFinder finder = new SubstringFinder(builder.toString().toLowerCase());
        System.out.println("готово.");

        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Введите искомую подстроку: ");
            Integer[] found = finder.findAll(sc.nextLine().toLowerCase());
            if (found.length > 0) {
                System.out.println("Индексы вхождений строки: " + Arrays.toString(found));
            } else {
                System.out.println("Ничего не найдено");
            }

            System.out.print("Искать еще? (y/n) ");
            while (true) {
                String answer = sc.nextLine().trim();
                if (answer.matches("[Yy]")) break;
                else if (answer.matches("[Nn]")) return;
                else System.out.print("Введите y или n: ");
            }
        }
    }
}
