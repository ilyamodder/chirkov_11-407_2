import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Класс, который ищет подстроки в строке используя суффиксный массив и бинарный поиск по нему
 * @author Илья Чирков
 */
public class SubstringFinder {
    private Integer[] suffixArray;
    private String baseString;

    /**
     * Инициализация класса, построение суффискного массива
     * @param baseString строка, по которой будет вестись поиск
     */
    public SubstringFinder(String baseString) {
        this.baseString = baseString;
        suffixArray = suffixArray(baseString);
    }

    /**
     * Возвращает построенный суффиксный массив
     * @param s строка, по которой нужно построить этот массив
     * @return суффиксный массив для данной строки
     */
    private Integer[] suffixArray(String s) {
        Integer[] suffixArr = new Integer[s.length()];
        for (int i = 0; i < suffixArr.length; i++) {
            suffixArr[i] = i;
        }
        Arrays.sort(suffixArr, (a, b) -> s.substring(a).compareTo(s.substring(b)));
        return suffixArr;
    }

    /**
     * Возвращает построенный суффиксный массив
     * @return суффиксный массив
     */
    public Integer[] getSuffixArray() {
        return suffixArray;
    }

    /**
     * Находит первое вхождение подстроки
     * @param s подстрока для поиска
     * @return индекс вхождения в суффиксном массиве либо -1, если подстрока не найдена
     */
    private int findFirst(String s) {
        return search(s, 0, baseString.length() - 1);
    }

    /**
     * Производит поиск подстроки в строке
     * @param s подстрока для поиска
     * @param start начальный индекс суффиксного массива
     * @param end конечный индекс суффиксного массива
     * @return индекс вхождения в суффиксном массиве либо -1, если подстрока не найдена
     */
    private int search(String s, int start, int end) {
        if (start > end) return -1;
        if (start == end) {
            if (baseString.substring(suffixArray[start]).startsWith(s))
                return start;
            else return -1;
        }
        int mid = start + (end - start) / 2;
        int compareResult = baseString.substring(suffixArray[mid], suffixArray[mid] + s.length()).compareTo(s);
        if (compareResult >= 0) {
            return search(s, start, mid);
        } else return search(s, mid + 1, end);
    }

    /**
     * Производит поиск всех вхождений подстроки
     * @param s подстрока для поиска
     * @return массив индексов всех вхождений подстроки. Если он пуст, то подстрока не найдена.
     */
    public Integer[] findAll(String s) {
        List<Integer> resultList = new ArrayList<>();
        int first = findFirst(s);
        if (first < 0) return new Integer[0];
        resultList.add(suffixArray[first]);
        for (int i=first + 1; i<suffixArray.length; i++) {
            if (baseString.substring(suffixArray[i]).startsWith(s)) {
                resultList.add(suffixArray[i]);
            } else break;
        }
        return resultList.toArray(new Integer[resultList.size()]);
    }
}
