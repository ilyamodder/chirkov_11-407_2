package ru.kpfu.chirkov.game2048;

import ru.kpfu.chirkov.game2048.model.*;
import ru.kpfu.chirkov.game2048.ui.UI;
import ru.kpfu.chirkov.game2048.ui.UIFactory;

import java.io.*;
import java.util.*;

/**
 * Main game class
 */
public class Game {
    private UI ui;
    private Field field;
    private File saveFile;
    private int startTiles;
    private int score;
    private int highScore;
    private boolean over;
    private boolean won;
    private boolean keepPlaying;

    /**
     * Game constructor
     * @param uiName name of UI, see UIFactory for details
     * @param size size of the square field
     */
    public Game(String uiName, int size) {
        ui = UIFactory.getUI(uiName);
        field = new Field(size);
        saveFile = new File("my.sav");
        startTiles = 2;

        score = 0;
        over = false;
        won = false;
        keepPlaying = false;

        // Add the initial tiles
        addStartPoints();

        try {
            load();
        } catch (IOException e) {
            System.out.println("Ошибка загрузки игры");
        }
    }

    /**
     * Start the game
     */
    public void startGame() {
        ui.draw(field, score, highScore);
        readCommands();
    }

    /**
     * Restart the game
     */
    public void restartGame() {
        score = 0;
        field = new Field(field.getMatrix().length);
        won = false;
        over = false;
        keepPlaying = false;
        addStartPoints();
        try {
            save();
        } catch (IOException e) {
            System.out.println("Ошибка сохранения игры");
        }
        refresh();
    }

    /**
     * Check whether game is terminated
     * @return state: true = terminated, else false
     */
    private boolean isGameTerminated() {
        return over || (won && !keepPlaying);
    }

    /**
     * Reading and processing user's commands
     */
    private void readCommands() {
        boolean flag = true;
        while (flag) {
            ui.showMessage("Введите команду");
            Event event = ui.getEvent();
            if (event == null) {
                ui.showMessage("Неизвестная команда");
                continue;
            }
            switch (event) {
                case UP:
                    move(new Position(0, -1));
                    break;
                case DOWN:
                    move(new Position(0, 1));
                    break;
                case LEFT:
                    move(new Position(-1, 0));
                    break;
                case RIGHT:
                    move(new Position(1, 0));
                    break;
                case RESTART:
                    restartGame();
                    break;
                case EXIT:
                    flag = false;
            }
        }

    }

    /**
     * Saves the game
     * @throws IOException
     */
    private void save() throws IOException {
        if (!saveFile.exists()) {
            saveFile.createNewFile();
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter(saveFile));

        Point[][] matrix = field.getMatrix();
        writer.write(score + " " + highScore + " " + matrix.length);
        writer.newLine();


        for (int i = 0; i < matrix[0].length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                Point current = matrix[j][i];
                writer.write((current != null ? current.getValue() : 0) + " ");
            }
            writer.newLine();
        }
        writer.flush();
        writer.close();
    }

    /**
     * Loads the game
     * @throws IOException
     */
    public void load() throws IOException {
        if (!saveFile.exists()) {
            saveFile.createNewFile();
            return;
        }
        BufferedInputStream stream = new BufferedInputStream(new FileInputStream(saveFile));
        Scanner input = new Scanner(stream);
        score = input.nextInt();
        highScore = input.nextInt();
        int size = input.nextInt();
        input.nextLine();
        Point[][] matrix = new Point[size][size];
        for (int i = 0; i < matrix[0].length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                int loaded = input.nextInt();
                if (loaded > 0) matrix[j][i] = new Point(new Position(j, i), loaded);
            }
        }
        field = new Field(matrix);

    }

    /**
     * Adds start points
     */
    private void addStartPoints() {
        for (int i = 0; i < startTiles; i++) {
            addRandomPoint();
        }
    }

    /**
     * Adds a random point to the field. 90% - 2, 10% - 4;
     */
    private void addRandomPoint() {
        if (field.getAvailablePointsCount() > 0) {
            int value = Math.random() < 0.9 ? 2 : 4;
            Point point = new Point(field.getRandomAvailablePoint(), value);
            field.putPoint(point);
        }
    }

    /**
     * Refreshes the UI
     */
    private void refresh() {
        if (score > highScore) highScore = score;
        ui.draw(field, score, highScore);
        try {
            save();
        } catch (IOException e) {
            ui.showMessage("Ошибка сохранения");
        }
        if (over) ui.showMessage("Вы проиграли!");
        else if (won && !keepPlaying) {
            ui.showMessage("Вы выиграли! Можете продолжить игру, если хотите.");
            keepPlaying = true;
        }
    }

    /**
     * Prepares the points: removes 'merged from' information
     */
    private void preparePoints() {
        Point[][] matrix = field.getMatrix();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] != null) {
                    matrix[i][j].setMergedFrom(null);
                    matrix[i][j].savePosition();
                }
            }
        }
    }

    /**
     * Moves the point to desired position
     * @param point point to move
     * @param position new position of the point
     */
    private void moveTile(Point point, Position position) {
        field.getMatrix()[point.getX()][point.getY()] = null;
        field.getMatrix()[position.getX()][position.getY()] = point;
        point.updatePosition(position);
    }

    /**
     * Makes a move
     * @param vector the direction to move to
     */
    private void move(Position vector) {

        if (this.isGameTerminated()) return; // Don't do anything if the game's over

        Traversals traversals = buildTraversals(vector);
        boolean moved = false;
        Position cell;
        Point tile;

        // Save the current tile positions and remove merger information
        preparePoints();

        // Traverse the grid in the right direction and move tiles
        for (int x : traversals.getX()) {
            for (int y : traversals.getY()) {
                cell = new Position(x, y);
                tile = field.getPoint(cell);

                if (tile != null) {
                    FahrestPosition positions = findFarthestPosition(cell, vector);
                    Point next = field.getPoint(positions.getNext());

                    // Only one merger per row traversal?
                    if (next != null && next.getValue() == tile.getValue() && next.getMergedFrom() == null) {
                        Point merged = new Point(positions.getNext(), tile.getValue() * 2);
                        //merged.setMergedFrom(new Point(tile, next));
                        merged.setMergedFrom(tile);

                        field.putPoint(merged);
                        field.removePoint(tile);

                        // Converge the two tiles' positions
                        tile.updatePosition(positions.getNext());

                        // Update the score
                        score += merged.getValue();

                        // The mighty 2048 tile
                        if (merged.getValue() == 2048) won = true;
                    } else {
                        moveTile(tile, positions.getFahrest());
                    }

                    if (!arePositionsEqual(cell, tile)) {
                        moved = true; // The tile moved from its original cell!
                    }
                }
            }
        }

        if (moved) {
            this.addRandomPoint();

            if (!this.areMovesAvailable()) {
                this.over = true; // Game over!
            }

            this.refresh();
        }
    }

    /**
     * Builds the traversals
     * @param vector the direction to move
     * @return traversals
     */
    private Traversals buildTraversals(Position vector) {
        Traversals traversals = new Traversals();
        for (int pos = 0; pos < field.getMatrix().length; pos++) {
            traversals.getX().add(pos);
            traversals.getY().add(pos);
        }

        // Always traverse from the farthest cell in the chosen direction
        List<Integer> x = traversals.getX();
        List<Integer> y = traversals.getY();
        if (vector.getX() == 1) Collections.reverse(x);
        if (vector.getY() == 1) Collections.reverse(y);

        return traversals;
    }

    /**
     * Checks whether the moves are available
     * @return moves are available or not
     */
    private boolean areMovesAvailable() {
        return field.getAvailablePointsCount() > 0 || areTileMatchesAvailable();
    }

    /**
     * Checks whether we can match the points
     * @return matches are available or not
     */
    private boolean areTileMatchesAvailable() {

        Point point;

        for (int i = 0; i < field.getMatrix().length; i++) {
            for (int j = 0; j < field.getMatrix().length; j++) {
                point = this.field.getPoint(new Position(i, j));

                if (point != null) {
                    Set<Position> vectors= new HashSet<>();
                    vectors.add(new Position(0, 1));
                    vectors.add(new Position(0, -1));
                    vectors.add(new Position(1, 0));
                    vectors.add(new Position(-1, 0));
                    for (Position vector : vectors) {
                        Position position = new Position(i + vector.getX(), j + vector.getY());

                        Point other = field.getPoint(position);

                        if (other != null && other.getValue() == point.getValue()) {
                            return true; // These two tiles can be merged
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Checks whether positions are equal
     * @param first first position
     * @param second second position
     * @return positions are equal or not
     */
    public boolean arePositionsEqual(Position first, Position second) {
        return first.getX() == second.getX() && first.getY() == second.getY();
    }

    public FahrestPosition findFarthestPosition(Position point, Position vector) {
        Position previous;

        // Progress towards the vector direction until an obstacle is found
        do {
            previous = point;
            point = new Position(previous.getX() + vector.getX(), previous.getY() + vector.getY());
        } while (field.isWithinBounds(point) &&
                field.isPointAvailable(point));

        return new FahrestPosition(previous, point);
    }
}


