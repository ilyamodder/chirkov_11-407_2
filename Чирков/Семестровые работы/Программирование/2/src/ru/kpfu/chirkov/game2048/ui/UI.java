package ru.kpfu.chirkov.game2048.ui;


import ru.kpfu.chirkov.game2048.model.Event;
import ru.kpfu.chirkov.game2048.model.Field;

/**
 * UI interface
 */
public interface UI {
    public void draw(Field field, int score, int highscore);
    public void showMessage(String text);
    public Event getEvent();
}
