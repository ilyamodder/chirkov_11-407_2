package ru.kpfu.chirkov.game2048;

/**
 * Created by ilya on 15.12.14.
 */
public class Main {
    public static void main(String[] args) {
        String uiName = "console";
        int size = 4;

        //parsing command line arguments
        for (int i = 0; i < args.length; i++) {
            if ((args[i].equals("-s") || args[i].equals("--size")) && (i < args.length - 1)) {
                try {
                    size = Integer.parseInt(args[i+1]);
                } catch (Exception e) {
                    System.out.println("Неверный формат размера поля. Посмотрите справку по --help");
                    return;
                }
            } else if ((args[i].equals("-i") || args[i].equals("--ui")) && (i < args.length - 1)) {
                uiName = args[i + 1];
            } else if (args[i].equals("-h") || args[i].equals("--help")) {
                System.out.println("Доступные параметры командной строки: \n" +
                        "-s (--size) n Задает размер игрового поля" +
                        "-h (--help) Вывод на экран этой справки\n" +
                        "-i (--ui) uiName Задает тип интерфейса игры, пока поддерживается только console\n" +
                        "\n" +
                        "Игровые команды\n" +
                        "up (w), down (s), left (a), right(d) Переход на шаг вверх, вниз, вправо и влево соотвественно\n" +
                        "exit Выход из игры");
                return;
            }
        }
        //creating and launching the game
        Game game = new Game(uiName, size);
        game.startGame();
    }
}
