package ru.kpfu.chirkov.game2048.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that represents the traversals
 */
public class Traversals {
    List<Integer> x;
    List<Integer> y;

    public Traversals() {
        x = new ArrayList<>();
        y = new ArrayList<>();
    }

    public List<Integer> getX() {
        return x;
    }

    public List<Integer> getY() {
        return y;
    }

    public void setX(List<Integer> x) {
        this.x = x;
    }

    public void setY(List<Integer> y) {
        this.y = y;
    }
}
