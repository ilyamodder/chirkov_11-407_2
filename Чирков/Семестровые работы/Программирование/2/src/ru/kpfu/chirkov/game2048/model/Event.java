package ru.kpfu.chirkov.game2048.model;

/**
 * Enum that represents user input events
 */
public enum Event {
    UP, DOWN, LEFT, RIGHT, RESTART, EXIT;
}
