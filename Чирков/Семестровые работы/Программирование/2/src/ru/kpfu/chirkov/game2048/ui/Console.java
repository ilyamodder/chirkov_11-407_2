package ru.kpfu.chirkov.game2048.ui;

import ru.kpfu.chirkov.game2048.model.Point;
import ru.kpfu.chirkov.game2048.model.Event;
import ru.kpfu.chirkov.game2048.model.Field;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Console interface
 */
public class Console implements UI {
    private static final int POINT_WIDTH = 6; //because max value of the field in 2048 is 131072: http://habrahabr.ru/post/219497/
    Scanner input;
    Map<String, Event> events;
    public Console() {
        input = new Scanner(System.in);
        events = new HashMap<>();
    }

    /**
     * Draws the field on console
     * @param field field
     * @param score current score
     * @param highscore current highscore
     */
    @Override
    public void draw(Field field, int score, int highscore) {
        StringBuilder screenBuilder = new StringBuilder();
        //add some new lines as fake clearing the screen
        for (int i = 0; i < 100; i++) {
            screenBuilder.append('\n');
        }
        screenBuilder.append("Счет: ").append(score).append("\tРекорд: ").append(highscore).append('\n');
        Point[][] matrix = field.getMatrix();

        appendDividerLine(screenBuilder, matrix.length, '╔', '╦', '╗');

        for (int i = 0; i < matrix.length; i++) {
            appendEmptyLine(screenBuilder, matrix.length);
            screenBuilder.append('║');
            for (int j = 0; j < matrix.length; j++) {
                Point current = matrix[j][i];
                appendIntToString(screenBuilder, (current == null ? 0 : current.getValue()));
                screenBuilder.append('║');
            }
            screenBuilder.append('\n');
            appendEmptyLine(screenBuilder, matrix.length);
            if (i < matrix.length - 1) appendDividerLine(screenBuilder, matrix.length, '╠', '╬', '╣');
        }

        appendDividerLine(screenBuilder, matrix.length, '╚', '╩', '╝');
        System.out.println(screenBuilder.toString());
    }

    /**
     * Prints a message to console
     * @param text text of the message
     */
    @Override
    public void showMessage(String text) {
        System.out.println(text);
    }

    /**
     * Waits for the event, then returns it .
     * @return an event
     */
    @Override
    public Event getEvent() {
        switch (input.nextLine()) {
            case "w":
            case "up":
                return Event.UP;
            case "s":
            case "down":
                return Event.DOWN;
            case "a":
            case "left":
                return Event.LEFT;
            case "d":
            case "right":
                return Event.RIGHT;
            case "exit":
                return Event.EXIT;
            case "new":
            case "restart":
                return Event.RESTART;
            default:
                return null;
        }
    }

    private void appendIntToString(StringBuilder builder, int a) {
        int length = 0;
        String number = a > 0 ? Integer.toString(a) : "";
        for (int i = 0; i < (POINT_WIDTH - number.length()) / 2; i++) {
            builder.append(" ");
            length++;
        }
        builder.append(number);
        length+=number.length();
        while (length < POINT_WIDTH) {
            builder.append(" ");
            length++;
        }
    }

    private void appendDividerLine(StringBuilder builder, int pointsCount,
                char start, char center, char end) {
        builder.append(start);
        for (int i = 0; i < pointsCount; i++) {
            for (int j = 0; j < POINT_WIDTH; j++) {
                builder.append('═');
            }
            if (i < pointsCount - 1) builder.append(center);
        }
        builder.append(end);
        builder.append('\n');
    }

    private void appendEmptyLine(StringBuilder builder, int pointsCount) {
        for (int i = 0; i < pointsCount; i++) {
            builder.append('║');
            for (int j = 0; j < POINT_WIDTH; j++) {
                builder.append(' ');
            }
        }
        builder.append('║');
        builder.append('\n');
    }
}
