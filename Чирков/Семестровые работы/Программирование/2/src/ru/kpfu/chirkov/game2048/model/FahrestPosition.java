package ru.kpfu.chirkov.game2048.model;

/**
 * Class that represents the result of Game.findFahrestPosition
 */
public class FahrestPosition {
    Position fahrest;
    Position next;

    public FahrestPosition(Position fahrest, Position next) {
        this.fahrest = fahrest;
        this.next = next;
    }

    public Position getFahrest() {
        return fahrest;
    }

    public Position getNext() {
        return next;
    }
}
