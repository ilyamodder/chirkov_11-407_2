package ru.kpfu.chirkov.game2048.ui;

/**
 * Created by ilya on 15.12.14.
 */
public class UIFactory {
    /**
     * Returns an UI object by the name
     * @param name name of the interface
     * @return an UI object if found, null otherwise
     */
    public static UI getUI(String name) {
        if ("console".equalsIgnoreCase(name)) {
            return new Console();
        }
        return null;
    }
}
