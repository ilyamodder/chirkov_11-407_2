package ru.kpfu.chirkov.game2048.model;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

/**
 * Game's field (matrix)
 */
public class Field {
    private Point[][] matrix;

    public Field(int size) {
        this(size, null);
    }

    public Field(int size, Field previousState) {
        matrix = previousState == null ? new Point[size][size] : previousState.getMatrix();
    }

    public Field(Point[][] matrix) {
        this.matrix = matrix;
    }

    /**
     * Clears the field
     */
    public void clear() {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = null;
            }
        }
    }

    /**
     * Gets a random available point
     * @return point
     */
    public Position getRandomAvailablePoint() {
        Set<Position> points = getAvailablePoints();
        Position point = null;
        if (points.size() > 0) {
            Iterator<Position> it = points.iterator();
            for (int i = 0; i <= new Random().nextInt(points.size()); i++) {
                point = it.next();
            }
        }
        return point;
    }

    /**
     * Gets all available points
     * @return available points
     */
    public Set<Position> getAvailablePoints() {
        Set<Position> points = new HashSet<>();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] == null) points.add(new Position(i, j));
            }
        }
        return points;
    }

    /**
     * Counts the available points and returns the count
     * @return count of availablke points
     */
    public int getAvailablePointsCount() {
        return getAvailablePoints().size();
    }

    /**
     * Returns whether a point is available or not
     * @param point point to check
     * @return true if the point is available, false otherwise
     */
    public boolean isPointAvailable(Position point) {
        return !isPointOccupied(point);
    }

    /**
     * Returns whether a point is occupied or not
     * @param point point to check
     * @return true if the point is occupied, false otherwise
     */
    public boolean isPointOccupied(Position point) {
        return getPoint(point) != null;
    }

    /**
     * Gets a point of desired position
     * @param point position of the point
     * @return the point if it presents on the position, null if the position is not occupied or
     * not within bounds of the field
     */
    public Point getPoint(Position point) {
        if (isWithinBounds(point)) {
            return matrix[point.getX()][point.getY()];
        } else {
            return null;
        }
    }

    /**
     * Puts a point at the desired position
     * @param point position
     */
    public void putPoint(Point point) {
        matrix[point.getX()][point.getY()] = point;
    }

    /**
     * Removes a point at the desired position
     * @param point position
     */
    public void removePoint(Position point) {
        matrix[point.getX()][point.getY()] = null;
    }

    /**
     * Checks whether the position is within bounds of the field
     * @param position position to check
     * @return true if the position is within bounds, false otherwise
     */
    public boolean isWithinBounds(Position position) {
        return position.getX() >= 0 && position.getX() < matrix.length &&
                position.getY() >= 0 && position.getY() < matrix.length;
    }

    /**
     * Returns a matrix of the field
     * @return matrix
     */
    public Point[][] getMatrix() {
        return matrix;
    }
}
