package ru.kpfu.chirkov.game2048.model;

/**
 * Class that represents a posotion at the field
 */
public class Position {
    protected int x, y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
