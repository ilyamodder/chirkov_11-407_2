package ru.kpfu.chirkov.game2048.model;

import java.io.Serializable;

/**
 * Point at the game's field (e.g 2, 4, 8...)
 */
public class Point extends Position implements Serializable {

    private int value;
    private Position previousPosition;
    Point mergedFrom;

    public Point(Position position, int value) {
        super(position.getX(), position.getY());
        this.value = value;
        mergedFrom = null;
        previousPosition = null;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Position getPreviousPosition() {
        return previousPosition;
    }

    public Point getMergedFrom() {
        return mergedFrom;
    }

    public void setMergedFrom(Point mergedFrom) {
        this.mergedFrom = mergedFrom;
    }

    public void savePosition() {
        previousPosition = new Position(x, y);
    }

    public void updatePosition(Position position) {
        x = position.getX();
        y = position.getY();
    }
}
