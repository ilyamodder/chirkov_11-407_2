package ru.kpfu.chirkov;

import ru.kpfu.chirkov.view.ConsoleView;

/**
 * Main class
 */
public class Main {
    public static void main(String[] args) {
        new ConsoleView().show();
    }
}
