package ru.kpfu.chirkov.model;

import ru.kpfu.chirkov.model.point.Point;
import ru.kpfu.chirkov.model.point.Position;
import ru.kpfu.chirkov.model.point.ShipPart;
import ru.kpfu.chirkov.model.point.WaterPoint;

import java.util.HashSet;
import java.util.Set;

/**
 * Class that represents game's grid
 */
public class Grid {
    private Point[][] points;
    private Set<Ship> ships;

    public Grid(int size) {
        points = new Point[size][size];
        ships = new HashSet<>();
        clearPoints();
    }

    /**
     * Clears the grid, sets all the points as WaterPoints
     */
    private void clearPoints() {
        for (int i = 0; i < points.length; i++) {
            for (int j = 0; j < points.length; j++) {
                points[i][j] = new WaterPoint(j, i);
            }
        }
    }

    /**
     * Adds point to the grid
     * @param point point to add
     * @param isLastOfShip if true, finds the ship relevant tothe point and adds it
     * */
    public void add(Point point, boolean isLastOfShip) {
        points[point.getY()][point.getX()] = point;
        if (isLastOfShip) {
            if (!(point instanceof ShipPart)) throw new IllegalArgumentException(point + " is not a ShipPart, it cannot be last");
            Ship ship = findShip((ShipPart) point);
            ships.add(ship);
            markAroundAsShot(ship);
        }
    }

    /**
     * Adds point to the grid
     * @param point point to add
     */
    public void add(Point point) {
        add(point, false);
    }


    private Ship findShip(ShipPart shipPart) {
        Ship ship = new Ship(shipPart);
        ship.addAll(getShipPartNeighbors(shipPart, new Position(1, 0)));
        ship.addAll(getShipPartNeighbors(shipPart, new Position(-1, 0)));
        ship.addAll(getShipPartNeighbors(shipPart, new Position(0, 1)));
        ship.addAll(getShipPartNeighbors(shipPart, new Position(0, -1)));
        return ship;
    }

    private Set<ShipPart> getShipPartNeighbors(ShipPart part, Position vector) {
        Set<ShipPart> parts = new HashSet<>();
        Point current = part;
        while (isWithinBounds(current, vector)
                && (current = points[current.getY() + vector.getY()][current.getX() + vector.getX()]) instanceof ShipPart) {
            parts.add((ShipPart) current);
        }
        return parts;
    }

    /**
     * Checks whether the point within bounds of the grid
     * @param point point to check
     * @return true if the point within bounds, false otherwise
     */
    public boolean isWithinBounds(Point point) {
        return (point.getY() >= 0) && (point.getX() >= 0) && (point.getX() < points.length) && (point.getY() < points.length);
    }

    /**
     * Checks whether both point and point moved by vector within bounds of the grid
     * @param point point to check
     * @param vector vector to check
     * @return true if the point within bounds, false otherwise
     */
    public boolean isWithinBounds(Point point, Position vector) {
        return isWithinBounds(point) && isWithinBounds(new Point(point.getX() + vector.getX(), point.getY() + vector.getY()));
    }

    private void markAroundAsShot(Ship ship) {
        ship.forEachPart(shipPart -> {
            for (int i=Math.max(0, shipPart.getY() - 1); i<Math.min(points.length, shipPart.getY() + 2); i++) {
                for (int j=Math.max(0, shipPart.getX() - 1); j<Math.min(points.length, shipPart.getX() + 2); j++) {
                    if (i == shipPart.getY() && j == shipPart.getX()) continue;
                    if (points[i][j] instanceof WaterPoint) points[i][j].shoot();
                }
            }
        });
    }

    /**
     * Returns the point at position
     * @param position position
     * @return point at desired position
     */
    public Point get(Position position) {
        return points[position.getY()][position.getX()];
    }

    /**
     * Returns the ship that corresponds the part
     * @param part part of the ship
     * @return ship
     */
    public Ship getShip(ShipPart part) {
        final Ship[] foundShip = new Ship[1];
        ships.forEach(ship -> {
            if (ship.contains(part)) foundShip[0] = ship;
        });
        return foundShip[0];
    }

    /**
     * Returns the size of the grid
     * @return size
     */
    public int size() {
        return points.length;
    }

    /**
     * Creates the ships based on points
     */
    public void createShips() {
        for (int i=0; i<size(); i++) {
            Set<ShipPart> parts = new HashSet<>();
            for (int j=0; j<size(); j++) {
                if (points[i][j] instanceof ShipPart) parts.add((ShipPart) points[i][j]);
                else if ((parts.size() > 1) ||
                        ((parts.size() == 1) && (((i==0) && points[i+1][j-1] instanceof WaterPoint) ||
                                ((i == size() - 1) && points[i-1][j-1] instanceof WaterPoint) ||
                                (points[i-1][j-1] instanceof WaterPoint && points[i+1][j-1] instanceof WaterPoint)))) {
                    ships.add(new Ship(parts));
                    parts = new HashSet<>();
                }
            }
        }
        for (int j=0; j<size(); j++) {
            Set<ShipPart> parts = new HashSet<>();
            for (int i=0; i<size(); i++) {
                if (points[i][j] instanceof ShipPart) parts.add((ShipPart) points[i][j]);
                else if ((parts.size() > 1) ||
                        ((parts.size() == 1) && (((j==0) && points[i-1][j+1] instanceof WaterPoint) ||
                                ((j == size() - 1) && points[i-1][j-1] instanceof WaterPoint) ||
                                (points[i-1][j-1] instanceof WaterPoint && points[i-1][j+1] instanceof WaterPoint)))) {
                    ships.add(new Ship(parts));
                    parts = new HashSet<>();
                }
            }
        }
    }

    /**
     * Checks whether all the ships are destroyed
     * @return true if they all destroyed, false otherwise
     */
    public boolean areAllShipsDestroyed() {
        final boolean[] areDestroyed = new boolean[1];
        areDestroyed[0] = true;
        ships.forEach(ship -> {
            if(!ship.isDestroyed()) areDestroyed[0] = false;
        });
        return areDestroyed[0];
    }

    /**
     * Calculates the count of the ships
     * @return coint of the ships
     */
    public int shipsCount() {
        return ships.size();
    }
}
