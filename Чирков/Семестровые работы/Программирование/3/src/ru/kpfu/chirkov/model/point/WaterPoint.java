package ru.kpfu.chirkov.model.point;

/**
 * Class that represent water point
 */
public class WaterPoint extends Point {
    public WaterPoint(int x, int y) {
        super(x, y);
    }

    public WaterPoint(int x, int y, boolean isHitted) {
        super(x, y, isHitted);
    }

    @Override
    public String toString() {
        return isHitted ? "•" : " ";
    }
}
