package ru.kpfu.chirkov.model.point;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Class that represents position (x, y)
 */
public class Position {
    private int x, y;

    private static Map<Character, Integer> charToInt;

    static {
        charToInt = new HashMap<>();
        charToInt.put('а', 0);
        charToInt.put('б', 1);
        charToInt.put('в', 2);
        charToInt.put('г', 3);
        charToInt.put('д', 4);
        charToInt.put('е', 5);
        charToInt.put('ж', 6);
        charToInt.put('з', 7);
        charToInt.put('и', 8);
        charToInt.put('к', 9);
    }

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Returns the x coordinate
     * @return the x coordinate
     */
    public int getX() {
        return x;
    }

    /**
     * Returns the y coordinate
     * @return the y coordinate
     */
    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Point) o;
        return Objects.equals(x, position.x) &&
                Objects.equals(y, position.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    /**
     * Factory method, Creates the position from user answer (e.g. и6)
     * @param answer user's answer
     * @return position from that string
     */
    public static Position fromString(String answer) {
        int x = charToInt.get(answer.charAt(0));
        int y = Integer.parseInt(answer.substring(1)) - 1;
        return new Position(x, y);
    }
}
