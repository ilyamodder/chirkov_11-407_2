package ru.kpfu.chirkov.model;

import ru.kpfu.chirkov.model.point.Point;
import ru.kpfu.chirkov.model.point.ShipPart;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Class that represent ship
 */
public class Ship {
    private Set<ShipPart> parts;

    /**
     * Constructor
     * @param part part of the ship that will be added to it
     */
    public Ship(ShipPart part) {
        this();
        parts.add(part);
    }

    /**
     * Constructor
     */
    public Ship() {
        parts = new HashSet<>();
    }

    /**
     * Constructor
     * @param parts parts of the ship
     */
    public Ship(Set<ShipPart> parts) {
        this();
        this.parts.addAll(parts);
    }

    /**
     * Adds the part to the ship
     * @param part part to add
     */
    public void addPart(ShipPart part) {
        parts.add(part);
    }

    /**
     * Adds all parts of the set
     * @param parts parts to add
     */
    public void addAll(Set<ShipPart> parts) {
        this.parts.addAll(parts);
    }

    /**
     * Does the action on each part
     * @param action lambda
     */
    public void forEachPart(Consumer<ShipPart> action) {
        parts.forEach(action);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ship ship = (Ship) o;
        return Objects.equals(parts, ship.parts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(parts);
    }

    /**
     * Check whether ship contains part
     * @param part part to check
     * @return true if the ship contains part, false otherwise
     */
    public boolean contains(ShipPart part) {
        return parts.contains(part);
    }

    /**
     * Checks whether all parts of the ship are destroyed
     * @return true if the ship is destroyed, false otherwise
     */
    public boolean isDestroyed() {
        final boolean[] isDestroyed = {true};
        parts.forEach(part -> {
            if (!part.isHitted()) isDestroyed[0] = false;
        });
        return isDestroyed[0];
    }
}
