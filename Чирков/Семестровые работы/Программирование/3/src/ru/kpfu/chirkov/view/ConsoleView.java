package ru.kpfu.chirkov.view;

import ru.kpfu.chirkov.model.EventListener;
import ru.kpfu.chirkov.model.Game;
import ru.kpfu.chirkov.model.Grid;
import ru.kpfu.chirkov.model.point.Point;
import ru.kpfu.chirkov.model.point.Position;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Scanner;

/**
 * Console view
 */
public class ConsoleView implements EventListener {
    Scanner input;
    Game game;
    public ConsoleView() {
        input = new Scanner(System.in);
        game = new Game();
        game.setEventListener(this);
    }

    /**
     * Shows a console view
     */
    public void show() {
        while (true) {
            try {
                game.loadFromFile(askTheUser("Введите имя файла с расстановкой кораблей", ".*"));
                break;
            } catch (IOException e) {
                System.out.println("Не удалось считать корабли из файла: " + e.getMessage());
            }
        }
        String answer = askTheUser("Создать сервер (s) или подключиться к существующему (c) ?", "^s|c$");
        if (answer.equals("s")) {
            System.out.println("Ждем подключения...");
            try {
                game.waitForClient();
            } catch (IOException e) {
                System.out.println("Ошибка сети: " + e.getMessage());
            }

        } else if (answer.equals("c")) {
            while (true) {
                String ip = askTheUser("Введите ip-адрес сервера: " , ".*");
                try {
                    game.connect(InetAddress.getByName(ip));
                    break;
                } catch (IOException e) {
                    System.out.println("Не удалось подключиться к серверу: " + e.getMessage());
                }
            }
        }
        try {
            game.start();
        } catch (IOException e) {
            System.out.println("Сетевая ошибка");
        }
    }

    private String askTheUser(String message, String regex) {
        while (true) {
            System.out.print(message + " ");
            String answer = input.nextLine();
            if (answer.matches(regex)) return answer;
            else System.out.println("Вы должны ввести строку, удовлетворяющую выражению " + regex);
        }
    }

    @Override
    public Position onMakeStep() {
        System.out.println("Ваш ход:");
        String answer = askTheUser("Введите координаты точки, куда вы хотите выстрелить (напрммер, А4) :",
                "^[а-яА-Я]([1-9]|10)$");
        return Position.fromString(answer);
    }

    @Override
    public void onUpdate(Game game) {
        printGrid(game.getMyGrid());
        System.out.println();
        printGrid(game.getPartnerGrid());
    }

    @Override
    public void onWon(Game game) {
        System.out.println("Вы выиграли!");
    }

    @Override
    public void onLose(Game game) {
        System.out.println("Вы проиграли!");
    }

    private void printGrid(Grid grid) {
        System.out.println("  а б в г д е ж з и к");
        for (int y = 0; y < grid.size(); y++) {
            System.out.printf("%02d", y+1);
            for (int x = 0; x < grid.size(); x++) {
                Point p = grid.get(new Position(x, y));
                System.out.print((p == null ? " " : p) + " ");
            }
            System.out.println();
        }
    }
}
