package ru.kpfu.chirkov.model.point;

/**
 * Class that represent game field's point
 */
public class Point extends Position {
    boolean isHitted;

    /**
     * Constructor
     * @param x x coordinate
     * @param y y coordinate
     */
    public Point(int x, int y) {
        super(x, y);
        isHitted = false;
    }

    /**
     * Constructor
     * @param x x coordinate
     * @param y y coordinate
     * @param isHitted is point hitted
     */
    public Point(int x, int y, boolean isHitted) {
        this(x, y);
        this.isHitted = isHitted;
    }

    /**
     * Sets the point as hitted
     */
    public void shoot() {
        isHitted = true;
    }

    /**
     * Chech whether the point is neighbor of positioin
     * @param position position to check
     * @return true if neighbor, false otherwise
     */
    public boolean isNeighborOf(Position position) {
        return !equals(position) && Math.abs(getX() - position.getX()) <= 1 && Math.abs(getY() - position.getY()) <= 1;
    }

    /**
     * Factory method that creates the point by character (# - water, * - ship)
     * @param symbol symbol
     * @param x x coordinate of point
     * @param y y coordinate of point
     * @return new WaterPoint or ShipPart
     * @throws UnsupportedOperationException if the symbol is unknown
     */
    public static Point create(char symbol, int x, int y) {
        if (symbol == '#') {
            return new WaterPoint(x, y);
        } else if (symbol == '*') {
            return new ShipPart(x, y);
        }
        else throw new UnsupportedOperationException("Unknown symbol " + symbol);
    }

    /**
     * Checks whether the point is hitted or not
     * @return is point hitted
     */
    public boolean isHitted() {
        return isHitted;
    }
}
