package ru.kpfu.chirkov.model;

import ru.kpfu.chirkov.model.point.Point;
import ru.kpfu.chirkov.model.point.Position;
import ru.kpfu.chirkov.model.point.ShipPart;
import ru.kpfu.chirkov.model.point.WaterPoint;

import java.io.IOException;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Main game class
 */
public class Game {

    public static final int GRID_SIZE = 10;
    private static final byte SHOOT_HEADER = 1, SHOOT_ANSWER_HEADER = 2;
    private static final byte STATUS_MISS = 0, STATUS_HIT = 1, STATUS_KILLED = 2;
    private Grid myGrid;
    private Grid partnerGrid;

    private boolean isServer;
    private DatagramSocket receiveSocket;
    private DatagramSocket sendSocket;
    private int serverPort = 27015, clientPort = 27016;

    private boolean won = false;
    private boolean over = false;

    private EventListener listener;

    private boolean isStarted;

    public Game() {
        partnerGrid = new Grid(GRID_SIZE);
        myGrid = new Grid(GRID_SIZE);
        isStarted = false;
    }

    /**
     * Connects to server, sets current player as client
     * @param address address of the server
     * @throws IOException if network error occurs
     */
    public void connect(InetAddress address) throws IOException {
        sendSocket = new DatagramSocket();
        isServer = false;
        sendSocket.connect(address, serverPort);
        receiveSocket = new DatagramSocket(clientPort);
        DatagramPacket handshake = new DatagramPacket(new byte[0], 0);
        sendSocket.send(handshake);
    }

    /**
     * Waits for client to connect, sets current player as server. Blocking call
     * @throws IOException if network error occurs
     */
    public void waitForClient() throws IOException {
        sendSocket = new DatagramSocket();
        isServer = true;
        receiveSocket = new DatagramSocket(serverPort);
        DatagramPacket handshake = new DatagramPacket(new byte[0], 0);
        receiveSocket.receive(handshake);
        sendSocket.connect(handshake.getAddress(), clientPort);
    }

    /**
     * Starts the game. You should call this method after establishing a connection
     * @throws IOException if network error occurs
     */
    public void start() throws IOException {
        listener.onUpdate(this);
        loop();
    }

    /**
     * Starts the game loop (making steps, etc.)
     * @throws IOException if network error occurs
     */
    public void loop() throws IOException {
        if (isServer()) {
            makeTurn();
        }
        while (isGameRunning()) {
            receiveTurn();
            checkForGameOver();
            if (isGameRunning()) makeTurn(); else break;
            checkForGameOver();
        }
    }

    private void checkForGameOver() {
        if (myGrid.areAllShipsDestroyed()) {
            over = true;
            won = false;
            listener.onLose(this);
        } else if (partnerGrid.areAllShipsDestroyed() && (partnerGrid.shipsCount() == 10)) {
            over = true;
            won = true;
            listener.onWon(this);
        }
    }

    private void makeTurn() throws IOException {
        Position posToShoot = listener.onMakeStep();
        byte[] data = new byte[] {SHOOT_HEADER, (byte) posToShoot.getX(), (byte) posToShoot.getY()};
        DatagramPacket packet = new DatagramPacket(data, data.length);
        sendSocket.send(packet);
        packet = new DatagramPacket(new byte[2], 2);
        receiveAndCheckForOrigin(packet);
        data = packet.getData();
        if (data[0] == SHOOT_ANSWER_HEADER) {
            switch (data[1]) {
                case STATUS_MISS:
                    partnerGrid.add(new WaterPoint(posToShoot.getX(), posToShoot.getY(), true));
                    break;
                case STATUS_HIT:
                    partnerGrid.add(new ShipPart(posToShoot.getX(), posToShoot.getY(), true));
                    break;
                case STATUS_KILLED:
                    partnerGrid.add(new ShipPart(posToShoot.getX(), posToShoot.getY(), true), true);
                    break;
                default:
                    throw new IOException("unknown status: " + data[1]);

            }
        } else throw new IOException("unknown packet header:" + data[0]);
        listener.onUpdate(this);
        if (data[1] != STATUS_MISS) {
            checkForGameOver();
            if (isGameRunning()) makeTurn();
        }
    }

    private void receiveTurn() throws IOException {
        DatagramPacket packet = new DatagramPacket(new byte[3], 3);
        receiveAndCheckForOrigin(packet);
        byte[] data = packet.getData();
        if (data[0] == SHOOT_HEADER) {
            Position shooted = new Position(data[1], data[2]);
            data = new byte[2];
            data[0] = SHOOT_ANSWER_HEADER;
            Point shootedPoint = myGrid.get(shooted);
            shootedPoint.shoot();
            if (shootedPoint instanceof WaterPoint) {
                data[1] = STATUS_MISS;
            } else if (shootedPoint instanceof ShipPart) {
                Ship ship = myGrid.getShip((ShipPart) shootedPoint);
                if (ship.isDestroyed()) {
                    data[1] = STATUS_KILLED;
                } else {
                    data[1] = STATUS_HIT;
                }
            }
            packet = new DatagramPacket(data, data.length);
            sendSocket.send(packet);
        }
        listener.onUpdate(this);
        if (data[1] != STATUS_MISS) {
            checkForGameOver();
            if (isGameRunning()) receiveTurn();
        }
    }

    private void receiveAndCheckForOrigin(DatagramPacket packet) throws IOException {
        do {
            receiveSocket.receive(packet);
        } while (!packet.getAddress().equals(sendSocket.getInetAddress()));
    }

    /**
     * Sets the EventListener
     * @param listener listener
     */
    public void setEventListener(EventListener listener) {
        this.listener = listener;
    }

    /**
     * Returns the player's grid
     * @return grid
     */
    public Grid getMyGrid() {
        return myGrid;
    }

    /**
     * Returns the partner's grid
     * @return partner's grid
     */
    public Grid getPartnerGrid() {
        return partnerGrid;
    }

    /**
     * Returns whether the player is server or not
     * @return true if the player is server, false otherwise
     */
    public boolean isServer() {
        return isServer;
    }

    /**
     * Loads ships fron file
     * @param filename name of the file. It should contain a 10x10 matrix with # as water, * as ship
     * @throws IOException if i/o error occurs
     */
    public void loadFromFile(String filename) throws IOException {
        if (!isStarted) {
            Set<ShipPart> parts = new HashSet<>();
            List<String> strings = Files.readAllLines(Paths.get(filename));
            for (int i = 0; i < strings.size(); i++) {
                char[] line = strings.get(i).toCharArray();
                for (int j = 0; j < line.length; j++) {
                    Point point = Point.create(line[j], j, i);
                    myGrid.add(point);
                    if (point instanceof ShipPart) parts.add((ShipPart) point);
                }
            }
            myGrid.createShips();
        }
    }

    /**
     * Returns whether the game is running (not over)
     * @return true if the game is running, false otherwise
     */
    public boolean isGameRunning() {
        return !over;
    }
}
