package ru.kpfu.chirkov.model.point;

/**
 * Class that represent part of the ship
 */
public class ShipPart extends Point {
    public ShipPart(int x, int y) {
        super(x, y);
    }

    public ShipPart(int x, int y, boolean isHitted) {
        super(x, y, isHitted);
    }

    @Override
    public String toString() {
        return isHitted ? "X" : "0";
    }
}
