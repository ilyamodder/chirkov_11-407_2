package ru.kpfu.chirkov.model;

import ru.kpfu.chirkov.model.point.Position;

/**
 * Listener interface for game callbacks
 */
public interface EventListener {
    Position onMakeStep();
    void onUpdate(Game game);

    void onWon(Game game);

    void onLose(Game game);
}
