package ru.kpfu.chirkov;

/**
 * Created by ilya on 28.03.15.
 */
public class Tree {

    private class Node {
        private Node left;
        private Node right;
        private double value;
        private int key;

        public Node(int key, double value) {
            this.value = value;
            this.key = key;
        }

        public Node getLeft() {
            return left;
        }

        public Node getRight() {
            return right;
        }

        public double getValue() {
            return value;
        }

        public int getKey() {
            return key;
        }

        public void setLeft(Node left) {
            this.left = left;
        }

        public void setRight(Node right) {
            this.right = right;
        }

        public void setValue(double value) {
            this.value = value;
        }
    }

    private Node root;

    public Tree() {}
    public void add(int key, double value) {
        Node newNode = new Node(key, value);
        if (root == null) {
            root = newNode;
        } else {
            add(root, newNode);
        }
    }

    private void add(Node root, Node node) {
        if (node.getKey() > root.getKey()) {
            if (root.getRight() == null) {
                root.setRight(node);
            } else {
                add(root.getRight(), node);
            }
        } else if (node.getKey() < root.getKey()) {
            if (root.getLeft() == null) {
                root.setLeft(node);
            } else {
                add(root.getLeft(), node);
            }
        } else {
            root.setValue(node.getValue());
        }
    }

    public void remove(int key) {
        Node parent = findParent(key);
        if (parent != null) {
            Node element = parent.getLeft();
            if (element.getKey() != key) element = parent.getRight();
            if (element.getKey() == key) {
                if (element.getLeft() == null) {
                    if (element.getRight() == null) {
                        parent.setLeft(null);
                    } else parent.setLeft(parent.getRight().getRight());
                } else {
                    if (element.getRight() == null) {
                        parent.setLeft(element.getLeft());
                    } else {
                        Node leaf = findLeaf(element.getRight(), element);
                        leaf.setLeft(element.getLeft());
                        leaf.setRight(element.getRight());
                        parent.setLeft(leaf);
                    }
                }
            }
        }
    }

    private Node findParent(int key){
        if ((root == null)||((root.getLeft() != null)&&(root.getLeft().getKey() == key))||((root.getRight() != null)&&(root.getRight().getKey() == key))) return root;
        else if(root.getKey() >= key){
            return find(root.getLeft(),key);
        } else return find(root.getRight(),key);
    }
    private Node find(Node root,int key){
        if ((root == null)||(root.getKey() == key)) return root;
        else if(root.getKey() >= key){
            return find(root.getLeft(),key);
        } else return find(root.getRight(),key);
    }

    private Node findLeaf(Node root,Node daddy){
        if ((root.getLeft() == null)&&(root.getRight() == null)){
            if (daddy.getLeft() == root) daddy.setRight(null);
            else daddy.setRight(null);
            return root;
        }
        else if(root.getLeft() == null){
            return findLeaf(root.getRight(),root);
        } else return findLeaf(root.getLeft(),root);
    }

    public double get(int key) {
        if (root == null) return Double.NaN;
        else {
            Node found = get(root, key);
            return found == null ? Double.NaN : found.getValue();
        }
    }

    private Node get(Node root, int key) {
        if ((root == null) || (root.getKey() == key)){
            return root;
        } else if (key > root.getKey()) {
            return get(root.getRight(), key);
        } else {
            return get(root.getLeft(), key);
        }
    }
}
