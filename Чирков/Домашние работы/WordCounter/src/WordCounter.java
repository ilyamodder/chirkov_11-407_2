import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ilya on 14.03.15.
 */
public class WordCounter {
    public static void main(String[] args) {
        File file = new File("file.txt");
        long start = System.currentTimeMillis();
        Map<String, Integer> counts = new HashMap<>();
        final int[] wordsCount = {0};
        try {
            List<String> lines = Files.readAllLines(file.toPath());
            lines.forEach(line -> {
                String[] words = line.toLowerCase().split("([ ,.?!()\t])+");
                if (line.length() > 0) {
                    for (String word : words) {
                        Integer count = counts.get(word);
                        counts.put(word, count == null ? 1 : count + 1);
                        wordsCount[0]++;
                    }
                }
            });
            System.out.println(counts);
            System.out.println("Количество слов:" + wordsCount[0]);
            System.out.println("Количество уникальных слов" + counts.size());
        } catch (IOException e) {
            System.out.println("Ошибка чтения из файла " + file.getAbsolutePath());
        }
        System.out.println(System.currentTimeMillis() - start);
    }
}
