package ru.kpfu.chirkov;

public class MyLinkedList<E> implements MyList<E> {
    private Link first;
    private Link last;
    private int size;

    public void add(E elem) {
        Link n = new Link(elem, null);
        if(first == null) {
            first = n;
            last = n;
        } else {
            last.setNext(n);
            last = n;
        }
        size++;
    }

    public void add(int index, E elem) {
        if(index > size || index < 0) {
            throw new IndexOutOfBoundsException("Index " + index + ", size is " + size);
        }
        Link x = first;
        Link prev = null;
        for (int i = 0; i < index; i++) {
            prev = x;
            x = x.next;
        }
        Link n = new Link(elem, x);
        if(prev != null) {
            prev.setNext(n);
        } else {
            first = n;
        }
        if(x == null) {
            last = n;
        }
        size++;
    }

    public void remove(int index) {
        if ((index >= size) || (index < 0)) throw new IndexOutOfBoundsException("Index " + index + ", size is " + size);
        Link x = first;
        Link prev = null;
        for (int i = 0; i < index; i++) {
            prev = x;
            x = x.getNext();
        }

        if(x.getNext() == null) {
            last = prev;
        }

        if(prev != null) {
            prev.setNext(x.getNext());
        } else {
            first = x.getNext();
        }
        size--;
    }

    public int size() {
        return size;
    }

    public E get(int index) {
        if ((index >= size) || (index < 0)) throw new IndexOutOfBoundsException("Index " + index + ", size is " + size);
        Link x = first;
        for (int i = 0; i < index; i++)
            x = x.getNext();
        return x.getValue();
    }

    private class Link {
        private E value;
        private Link next;

        public Link(E value, Link next) {
            this.value = value;
            this.next = next;
        }

        public E getValue() {
            return value;
        }

        public void setValue(E value) {
            this.value = value;
        }

        public Link getNext() {
            return next;
        }

        public void setNext(Link next) {
            this.next = next;
        }
    }
}
