import java.util.ArrayList;
import java.util.List;

/**
 * Created by ilya on 14.02.15.
 */
public class FareyLink implements Farey {
      public List<Number> farey(int a) {
        if (a == 1) {
            List<Number> numbers = new ArrayList<Number>();
            numbers.add(new Number(0, 1, 1));
            numbers.add(new Number(1, 1, 0));
            return numbers;
        } else {
            List<Number> numbers = farey(a - 1);
            int length = numbers.size();
            for (int i=0; i<length; i++) {
                Number left = numbers.get(i);
                Number right = numbers.get(left.getNext());
                if ((left.getNext() != 0) && (left.getQi() + right.getQi() <= a)) {
                    numbers.add(new Number(left.getPi() + right.getPi(), left.getQi() + right.getQi(), left.getNext()));
                    left.setNext(numbers.size() - 1);
                }
            }
            return numbers;
        }
    }
}
