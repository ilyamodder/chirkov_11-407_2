import java.util.List;

/**
 * Created by ilya on 27.02.15.
 */
public interface Farey {
    public List<Number> farey(int a);
}
