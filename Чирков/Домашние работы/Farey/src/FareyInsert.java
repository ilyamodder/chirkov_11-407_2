import java.util.ArrayList;
import java.util.List;

/**
 * Created by ilya on 14.02.15.
 */
public class FareyInsert implements Farey {
    public List<Number> farey(int a) {
        if (a == 1) {
            List<Number> numbers = new ArrayList<Number>();
            numbers.add(new Number(0, 1));
            numbers.add(new Number(1, 1));
            return numbers;
        } else {
            List<Number> numbers = farey(a-1);
            for (int i=0; i<numbers.size()-1; i+=2) {
                Number left = numbers.get(i);
                Number right = numbers.get(i+1);
                numbers.add(i + 1, new Number(left.getPi() + right.getPi(), left.getQi() + right.getQi()));
            }
            return numbers;
        }
    }


}
