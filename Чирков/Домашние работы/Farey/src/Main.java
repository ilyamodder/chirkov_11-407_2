import java.util.List;

/**
 * Created by ilya on 27.02.15.
 */
public class Main {
    public static void main(String[] args) {
//        System.out.println("Начинаем работу");
//        long start = System.currentTimeMillis();
//        List<Number> numbersInsert = FareyInsert.farey(20);
//        long timeInsert = System.currentTimeMillis() - start;
//        System.out.println("Алгоритм со вставками выполнился за " + timeInsert + " мс");
//
//        start = System.currentTimeMillis();
//        List<Number> numbersLink = FareyLink.farey(20);
//        long timeLink = System.currentTimeMillis() - start;
//        System.out.println("Алгоритм со связанным списком выполнился за " + timeLink + " мс");
//        for (Number number : numbersLink) {
//            System.out.print(number + " ");
//        }

        Farey[] impls = new Farey[2];
        impls[0] = new FareyLink();
        impls[1] = new FareyInsert();


        for (int i=0; i<impls.length; i++) {
            if (i==1) {
                System.out.println("Алгоритм со вставками:");
            } else {
                System.out.println("Алгоритм со связанным списком:");
            }
            for (int j = 1; j <=100; j++) {
                long start = System.currentTimeMillis();
                impls[i].farey(j);
                System.out.println(System.currentTimeMillis() - start);
            }
        }
    }
}
