import java.util.ArrayList;

/**
 * Created by ilya on 05.03.15.
 */
public class JavaArrayList<E> extends ArrayList<E> implements Benchmarkable{
    public JavaArrayList(int initialCapacity) {
        super(initialCapacity);
    }

    @Override
    public long[] bench(int count) {
        long[] result = new long[2];
        long start = System.currentTimeMillis();
        for (int j = 0; j < count; j++) {
            this.add(null);
        }
        result[0] = System.currentTimeMillis() - start;
        start = System.currentTimeMillis();
        for (int j=0; j<count; j++) {
            this.get(size() - 1);
            this.remove(size() - 1);
        }
        result[1] = System.currentTimeMillis() - start;
        return result;
    }

    @Override
    public String getName() {
        return ArrayList.class.toString();
    }
}
