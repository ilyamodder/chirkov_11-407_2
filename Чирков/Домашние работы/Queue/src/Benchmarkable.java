/**
 * Created by ilya on 05.03.15.
 */
public interface Benchmarkable {
    public long[] bench(int count);
    public String getName();
}
