/**
 * Created by ilya on 05.03.15.
 */
public interface Queue<E> {
    public void push(E object);

    public E poll();

    public E peek();

    public int size();

    public void addQueue(Queue<? extends E> queue);

    public void clear();
}
