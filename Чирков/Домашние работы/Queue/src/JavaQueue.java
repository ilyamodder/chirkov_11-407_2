import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by ilya on 05.03.15.
 */
public class JavaQueue<E> extends ArrayBlockingQueue<E> implements Benchmarkable {
    public JavaQueue(int capacity) {
        super(capacity);
    }

    @Override
    public long[] bench(int count) {
        long[] result = new long[2];
        long start = System.currentTimeMillis();
        for (int j = 0; j < count; j++) {
            this.add((E) new Object());
        }
        result[0] = System.currentTimeMillis() - start;
        start = System.currentTimeMillis();
        for (int j=0; j<count; j++) {
            this.poll();
        }
        result[1] = System.currentTimeMillis() - start;
        return result;
    }

    @Override
    public String getName() {
        return ArrayBlockingQueue.class.toString();
    }
}
