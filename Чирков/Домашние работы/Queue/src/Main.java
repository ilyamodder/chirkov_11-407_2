import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.management.BufferPoolMXBean;

/**
 * Created by ilya on 05.03.15.
 */
public class Main {
    public static final String DIVIDER = ",";

    public static void main(String[] args) {
        int[] iterations = {1000, 2_500, 5_000, 7_500, 10_000, 20_000, 30_000, 40_000, 50_000, 100_000, 200_000, 300_000, 400_000, 500_000};
        Benchmarkable[] algs = new Benchmarkable[4];
        long[][] benchResults = new long[iterations.length][algs.length*2];
        for (int i=0; i<iterations.length; i++) {
            algs[0] = new MyQueue<String>(iterations[i]);
            algs[1] = new JavaQueue<String>(iterations[i]);
            algs[2] = new MyArrayList<String>(iterations[i]);
            algs[3] = new JavaArrayList<String>(iterations[i]);
            for (int j=0; j< algs.length; j++) {
                System.out.printf("Тестируем коллекцию %s на %d элементах%n", algs[j].getName(), iterations[i]);
                long[] result = algs[j].bench(iterations[i]);
                benchResults[i][j*2] = result[0];
                benchResults[i][j*2+1] = result[1];
            }
        }

        StringBuilder builder = new StringBuilder();
        builder.append("Количество элементов");
        for (int i = 0; i < algs.length; i++) {
            builder.append(DIVIDER);
            builder.append(algs[i].getName());
            builder.append(" - добавление");
            builder.append(DIVIDER);
            builder.append(algs[i].getName());
            builder.append(" - удаление");
        }
        for (int i = 0; i < iterations.length; i++) {
            builder.append("\n");
            builder.append(iterations[i]);
            for (int j = 0; j < algs.length*2; j++) {
                builder.append(DIVIDER);
                builder.append(benchResults[i][j]);
            }
        }
        try (FileOutputStream stream = new FileOutputStream(new File("results.csv"))) {
            stream.write(builder.toString().getBytes("UTF-8"));
            System.out.println("Результаты сравнения производительности записаны в results.csv");
        } catch (IOException e) {
            System.out.println("Ошибка записи результатов в файл");
        }
    }
}
