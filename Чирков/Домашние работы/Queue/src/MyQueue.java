import java.util.*;

/**
 * Created by ilya on 05.03.15.
 */
public class MyQueue<E> implements Queue<E>, Benchmarkable {

    private Object[] data;
    private int size;

    public MyQueue() {
        clear();
    }

    public MyQueue(int size) {
        data = new Object[size];
        size = 0;
    }

    @Override
    public void push(E object) {
        if (data.length == size) {
            Object[] newArray = new Object[size * 2];
            System.arraycopy(data, 0, newArray, 0, size);
            data = newArray;
        }
        data[size++] = object;
    }

    @Override
    public E poll() {
        E obj = peek();
        System.arraycopy(data, 1, data, 0, size - 1);
        size--;
        data[size] = null;
        return obj;
    }

    @Override
    @SuppressWarnings("unchecked")
    public E peek() {
        return (E) data[0];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void addQueue(Queue<? extends E> queue) {
        while (queue.size() > 0) {
            push(queue.poll());
        }
    }

    @Override
    public void clear() {
        size = 0;
        data = new Object[10];
    }

    @Override
    public long[] bench(int count) {
        long[] result = new long[2];
        for (int i = 0; i < result.length; i++) {
            long start = System.currentTimeMillis();
            for (int j = 0; j < count; j++) {
                this.push(null);
            }
            result[0] = System.currentTimeMillis() - start;
            start = System.currentTimeMillis();
            for (int j=0; j<count; j++) {
                this.poll();
            }
            result[1] = System.currentTimeMillis() - start;
        }
        return result;
    }

    @Override
    public String getName() {
        return getClass().toString();
    }

}
